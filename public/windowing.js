let windowList = [];

let active = {}

let deletedFiles = []

let getEmail;

const selectRandom = (a) => {
    const selection = Math.floor(Math.random()*a.length);
    return a[selection];
}

let convertSize = (size) => {
    if (size > 1000000000) {
        return Math.ceil(size/100000000)/10+'G'
    } else if (size > 1000000) {
        return Math.ceil(size/100000)/10+'M'
    } else if (size > 1000) {
        return Math.ceil(size/100)/10+'K'
    } else {
        return Math.ceil(size)+'B';
    }
}

let conditions = {
    registration: false,
    commaWebR: false,
    commaWebD: false,
    doorsAPI: false,
    auditiveGallery: false,
}

class FOSFile {
    constructor(name, size, program) {
        this.size = size;
        this.name = name;
        this.program = program;
        this.listElem = document.createElement('div');
        this.listElem.classList.add('fsObject');
        this.listElem.ondblclick = () => {
            switch (this.name) {
                case "Unbelievable Engine Updater Installer.exe":
                    unbelievableEngineUpdaterInstaller(rootdir)
                    break;
                case 'Doors API Installer.exe':
                    doorsAPIInstaller(rootdir)
                    break;
                case 'Doors API.exe':
                    textPopup("DoorsAPI", "Doors API already installed :)")
                    break;
                case 'Comma Web Development Kit 4.2.3.exe':
                    textPopup("Comma Web Development Kit", "Warning, this version of Comma Web Development Kit is not compatible with your installation!")
                    break;
                case 'Comma Web Development Kit 6.x.exe':
                    commaWebInstaller(rootdir)
                    break;
                case 'Comma Web Development Kit.exe':
                    textPopup("Comma Web Development Kit", "Comma Web Development Kit already installed :)")
                    break;
                case 'Comma Web Runtime 2.x.exe':
                    textPopup("Comma Web Runtime", "Warning, this version of Comma Web Runtime is not compatible with your installation!")
                    break;
                case 'Comma Web Runtime 3.x.exe':
                    commaWebRuntimeInstaller(rootdir)
                    break;
                case 'Comma Web Runtime.exe':
                    textPopup("Comma Web Runtime", "Comma Web Runtime already installed :)")
                    break;
                case 'Auditive Gallery Community Edition Installer.exe':
                    auditiveGalleryInstaller(rootdir)
                    break;
                case 'Auditive Gallery Community Edition.exe':
                    textPopup("Auditive Gallery Community Edition", "Auditive Gallery Community Edition already installed :)")
                    break;
                case 'Unbelievable Engine Installer.exe':
                    unbelievableEngineInstaller(rootdir)
                    break;
                case 'Unbelievable Engine.exe':
                    winScreen(rootdir)
                    break;
                default: 
                    textPopup('Can not open file', 'You should be doing gamedev you know ;)')
            }
        };
        this.listElem.onclick = () => {this.select()};
        this.attach();
        this.listElem.addEventListener('contextmenu', (ev) => {
            ev.preventDefault();
            contextMenu({x: ev.clientX, y: ev.clientY} ,[
                {text: 'delete', callback:() => {
                    let popupContent = document.createElement('div');
                    popupContent.innerHTML = `Are you sure you want to delete file ${this.name}?`
                    popup(`Really delete ${this.name}?`, popupContent, [{text: 'Yes', handler: (close) => {this.delete(); close(true)}},{text: 'No', handler: (close) => close(true)}]);  
                }},
                {text: 'create folder', callback: () => {
                    let popupContent = document.createElement('div');
                    popupContent.innerHTML = `Folder name:`
                    let input = document.createElement('input');
                    input.type = 'text';
                    popupContent.appendChild(input);
                    popup(`Create folder`, popupContent, [{text: 'Create', handler: (close) => {
                        try {
                            this.parent.addChild(new Directory(input.value));close(true)
                        } catch (e) {
                            let popupContent = document.createElement('div');
                            popupContent.innerHTML = e;
                            popup("Could not create folder", popupContent);
                        }
                    }},{text: 'Cancel', handler: (close) => close(true)}]); 
                }},
            ]);
            return false;
        }, false) 
    }
    delete () {
        if ([
            'Comma Web Development Kit.exe',
            'Comma Web Runtime.exe',
            'Unbelievable Engine Installer.exe',
            'Unbelievable Engine.exe',
            'Auditive Gallery Community Edition.exe',
            'Doors API.exe',
        ].includes(this.name)) {
            textPopup('Error', 'Deleting this porgram can compromise system integrity, deletion prevented!')
            return
        }
        if (this.name == "Cursor changer.exe") {
            document.body.classList.remove('customCursor')
        }
        if (this.name == "Wallpaper.png") {
            desktop.classList.remove('background');
        }
        if (this.name == "Desktop Pet.exe") {
            document.getElementById('pet').classList.add('hidden');
        }
        if ([
            "My Favourite Feel Good Playlist",
            "Wedding photos",
            "Happy times",
            "Simple moments",
            "Cherished memories",
        ].includes(this.parent.name)){
            if(!deletedFiles.includes(this.parent.name)) {
                deletedFiles.push(this.parent.name);
            }
        }else {
            deletedFiles.push(this.name);
        }
        this.parent.removeChild(this);
    }
    select () {
        [...document.getElementsByClassName('selectedFile')].forEach(e => e.classList.remove('selectedFile'));
        this.listElem.classList.add('selectedFile');
    }
    attach (){
        this.listElem.innerHTML = `<img src='img/${
            (this.name.match('mp3') || this.name.match('Track'))?'music':
            (this.name.match('png') || this.name.match('IMG'))?'picture':
            (this.name.match('xls') || this.name.match('csv'))?'table':
            (this.name.match('zip') || this.name.match('tgz'))?'archive':
            (this.name.match('doc') || this.name.match('ods'))?'document':
            (this.name.match('mp4') || this.name.match('webm'))?'video':
            'web'
        }.png' /><div>${this.name}</div> <div> ${convertSize(this.size)} </div>`;
    }
}

class FOSWindow{
    constructor(name, size, position, content, icon) {
        this.window = document.createElement('div');
        this.window.classList.add('window')
        this.window.style.width = size.x;
        this.window.style.height = size.y;
        this.window.style.left = position.x;
        this.window.style.top = position.y;
        this.header = document.createElement('div');
        this.header.classList.add('header');
        this.header.innerHTML = name;
        this.closeButton = document.createElement('div');
        this.closeButton.classList.add('closeButton');
        this.closeButton.innerHTML = 'x';
        this.closeButton.onclick = (e) => {this.close(); e.stopImmediatePropagation()};
        this.header.appendChild(this.closeButton);
        this.window.appendChild(this.header);
        this.window.appendChild(content);
        this.window.onclick = () => this.activate();
        this.taskBarIcon = document.createElement('div');
        this.taskBarIcon.onclick = () => {this.activate()};
        this.taskBarIcon.appendChild(makeImage(icon));
        this.desktopIcon = document.createElement('div');
        this.desktopIcon.appendChild(makeImage(icon));
        this.desktopIcon.onclick = () => {this.activate()};
        this.desktopIcon.innerHTML += `<div>${name}</div>`;
        this.isActive = false;
        this.isOpen = false;
        this.dragSetup();
    }
    attach(windowGroup, taskBar, desktop, windowList) {
        windowGroup.appendChild(this.window);
        taskBar.appendChild(this.taskBarIcon);
        if (desktop) desktop.appendChild(this.desktopIcon);
        this.windowList = windowList;
    }
    activate (noisy = false) {
        const maxZ = Math.max(...this.windowList.map(e => e.window.style.zIndex))
        this.windowList.forEach(window => window.inactivate(maxZ));
        this.isActive = true;
        if (noisy) {
            this.window.classList.remove('noisyWindow');
            this.window.classList.add('noisyWindow');
        } 
        this.window.classList.add('activeWindow');
        this.window.style.zIndex = 1;
        this.open();
    }
    inactivate (zDrop) {
        this.window.classList.remove('activeWindow');
        this.isActive = false;
        this.window.style.zIndex -= zDrop;
    }
    close (kill) {
        this.window.style.display = 'none';
        this.isOpen = false;
        if (kill) {
            this.window.parentElement.removeChild(this.window);
            if (this.desktopIcon.parentElement) this.desktopIcon.parentElement.removeChild(this.desktopIcon);
            this.taskBarIcon.parentElement.removeChild(this.taskBarIcon);
        }
    }
    open () {
        this.window.style.display = null;
        this.isOpen = true;
    }

    dragSetup() {
        var pos1 = 0,
            pos2 = 0,
            pos3 = 0,
            pos4 = 0;
        
        if ("ontouchstart" in document.documentElement) {
            var pos1touch = 0,
                pos2touch = 0,
                pos3touch = 0,
                pos4touch = 0;
        }

    
        let dragMouseDown = (e) => {
            if (!"ontouchstart" in document.documentElement) {
                e.preventDefault();
            }
            pos3 = e.clientX;
            pos4 = e.clientY;
            if ("ontouchstart" in document.documentElement) {
                try {
                    pos3touch = e.touches[0].clientX;
                    pos4touch = e.touches[0].clientY;
                } catch(error) {}
            }
            document.onmouseup = closeDragElement;
            document.onmousemove = elementDrag;
            document.ontouchend = closeDragElement;
            document.ontouchmove = elementDrag;
            this.activate()
        }
    
        let elementDrag = (e) => {
            e.preventDefault();
            if ("ontouchstart" in document.documentElement) {
                pos1touch = pos3touch - e.touches[0].clientX;
                pos2touch = pos4touch - e.touches[0].clientY;
                pos3touch = e.touches[0].clientX;
                pos4touch = e.touches[0].clientY;
                this.window.style.top = (this.window.offsetTop - pos2touch) + "px";
                this.window.style.left = (this.window.offsetLeft - pos1touch) + "px";
            } else {
                pos1 = pos3 - e.clientX;
                pos2 = pos4 - e.clientY;
                pos3 = e.clientX;
                pos4 = e.clientY;
                this.window.style.top = (this.window.offsetTop - pos2) + "px";
                this.window.style.left = (this.window.offsetLeft - pos1) + "px";
            }
        }
    
        function closeDragElement() {
            document.onmouseup = null;
            document.onmousemove = null;
            document.ontouchend = null;
            document.ontouchmove = null;
        }
        if (this.header) {
            this.header.onmousedown = dragMouseDown;
            this.header.ontouchstart = dragMouseDown;
        }
    }
    
};

let popup = (title, insides, options = [{text: 'ok', handler: (callback) => callback(true)}]) => {
    const content = document.createElement('div');
    content.classList.add('Popup')
    let win = new FOSWindow(title, {x:`30em`, y:`15em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, content, 'img/error.png');
    win.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(win);
    content.appendChild(insides);
    options.forEach((option) => {
        element = document.createElement('button');
        element.innerHTML = option.text;
        element.onclick = () => option.handler((close) => {if (close) win.close(true);});
        content.appendChild(element)
    });
    win.activate(true);
    return () => win.close(true);
}

let textPopup = (title, insides, options = [{text: 'ok', handler: (callback) => callback(true)}]) => {
    let content = document.createElement('div');
    content.innerHTML = insides;
    return popup(title, content, options);
}

let makeImage = (url) => {
    const image = document.createElement('img');
    image.src = url;
    return image;
}

let alreadyExistsError = () => {
    return 'File already exists!'
}

let fileSystemFullError = (size) => {
    return `The file system is full! Free up at least ${convertSize(size)} more space! (Hint: You can right click files to delete them)`
}

let fileNotFoundError = (file) => {
    return `File ${file} not found`;
}

function confirmRegistration() {
    conditions.registration = true;
    textPopup("Registration confirmed", "Congratulations, you have successfully regsitered to use UnbelievableEngine!");
}

let contextMenu = (position, options = []) => {
    const contextMenu = document.createElement('div')
    contextMenu.classList.add('contextMenu')
    contextMenu.style.left = `${position.x}px`;
    contextMenu.style.top = `${position.y}px`;
    options.forEach(option => {
        const contextMenuItem = document.createElement('div');
        contextMenuItem.innerHTML = option.text;
        contextMenuItem.onclick = () => option.callback();
        contextMenu.appendChild(contextMenuItem);
    });
    document.children[0].appendChild(contextMenu);
    let clickListener = () => {
        contextMenu.parentElement.removeChild(contextMenu);
        document.removeEventListener('click', clickListener);
    }
    let eventListener = document.addEventListener('click', clickListener)
}

class Directory {
    constructor (name, parent, content = [], maxCapacity = -1){
        this.name = name;
        this.parent = parent;
        this.content = content;
        this.maxCapacity = maxCapacity;
        this.insideDisplay = document.createElement('div');
        this.insideDisplay.classList.add('fsWindow');
        this.listElem = document.createElement('div');
        this.listElem.classList.add('fsObject');
        this.listElem.innerHTML = `<img src='img/folder.png' /> <div>${this.name}</div> <div> ${convertSize(this.size)} </div>`;
        this.listElem.ondblclick = () => {this.attach(true)};
        this.listElem.onclick = () => {this.select()};
        this.listElem.addEventListener('contextmenu', (ev) => {
            ev.preventDefault();
            contextMenu({x: ev.clientX, y: ev.clientY} ,[
                {text: 'delete', callback:() => {
                    let popupContent = document.createElement('div');
                    popupContent.innerHTML = `Are you sure you want to delete folder ${this.name}?`
                    popup(`Really delete ${this.name}?`, popupContent, [{text: 'Yes', handler: (close) => {this.delete();close(true)}},{text: 'No', handler: (close) => close(true)}]);  
                }},
                {text: 'create folder', callback: () => {
                    let popupContent = document.createElement('div');
                    popupContent.innerHTML = `Folder name:`
                    let input = document.createElement('input');
                    input.type = 'text';
                    popupContent.appendChild(input);
                    popup(`Create folder`, popupContent, [{text: 'Create', handler: (close) => {
                        try {
                            this.parent.addChild(new Directory(input.value));close(true)
                        } catch (e) {
                            let popupContent = document.createElement('div');
                            popupContent.innerHTML = e;
                            popup("Could not create folder", popupContent);
                        }
                    }},{text: 'Cancel', handler: (close) => close(true)}]); 
                }},
            ]);
            return false;
        }, false) 
    }
    delete () {
        deletedFiles.push(this.name);
        this.parent.removeChild(this);
    }
    select () {
        [...document.getElementsByClassName('selectedFile')].forEach(e => e.classList.remove('selectedFile'));
        this.listElem.classList.add('selectedFile');
    }
    attach (active = false, container = this.container) {
        this.container = container;
        if (active) {
            this.container.innerHTML = '';
            this.container.appendChild(this.inside);
        }
    }
    get size () {
        return this.content.reduce((a,e) => a+e.size, 0);
    }
    get inside () {
        let root = this;
        while(root.parent){
            root = root.parent;
        }
        this.insideDisplay.innerHTML = `<div class="filesHeader">${convertSize(root.size)}/${convertSize(root.maxCapacity)}</div>`;
        if (this.parent) {
            const parentLink = document.createElement('div');
            parentLink.classList.add('fsObject');
            parentLink.innerHTML = `<img src='img/back.png' />..`;
            parentLink.onclick = () => this.parent.attach(true);
            this.insideDisplay.appendChild(parentLink)
        }
        this.content.map(e => e.listElem).forEach(i => this.insideDisplay.appendChild(i));
        return this.insideDisplay;
    }
    refresh() {
        this.listElem.innerHTML = `<img src='img/folder.png' /><div>${this.name}</div> <div> ${convertSize(this.size)} </div>`;
        this.inside;
        if (this.parent) {
            return this.parent.refresh();
        } else if (this.maxCapacity > 0 && this.maxCapacity < this.size) {
            return this.size - this.maxCapacity;
        } else {
            return 0;
        }
    }
    addChild (child) {
        if (this.getChild(child.name, false)) throw alreadyExistsError();
        child.parent = this;
        child.attach(false, this.container);
        this.content.push(child);
        if (this.refresh()) {
            let error = fileSystemFullError(this.refresh());
            this.removeChild(child);
            throw error
        } else {
            return true;
        }
    }
    removeChild (child) {
        child.parent = null;
        this.content = this.content.filter(e => e !== child);
        this.listElem.innerHTML = `<div>${this.name}</div> <div> ${convertSize(this.size)} </div>`;
        this.refresh();
    }
    getChild (name, error = true) {
        let file = this.content.find(e => e.name === name);
        if (file || !error) return file;
        else throw fileNotFoundError(name);
    }
}

let downloadFOSFile = new FOSFile('Download', 10000000, undefined);

function createDirStructure(dirstruct) {
    dirstruct.c.forEach( d => {
        if (d.dir) {
            dirstruct.dir.addChild(d.dir);
            createDirStructure(d);
        } else {
            dirstruct.dir.addChild(d.f)
        }
    });
    dirstruct.dir.inside;
}


function emailApp(rootdir) {
    let emailElem = document.createElement('div');
    const header = document.createElement('div');
    header.classList.add('emailHeader');
    const messagesArea = document.createElement('div');
    messagesArea.classList.add('messagesArea');
    const readerArea = document.createElement('div');
    readerArea.classList.add('readerArea');
    emailElem.appendChild(header);
    emailElem.appendChild(messagesArea);
    emailElem.appendChild(readerArea);
    emailElem.classList.add('emailApp');

    const sendButton = document.createElement('button');
    sendButton.innerHTML = 'Send Email';
    sendButton.onclick = () => {
        textPopup("Stop", "You should be doing gamedev ;)")
    }
    header.appendChild(sendButton)

    const emailWin = new FOSWindow(`Email`, {x:`50%`, y:`30em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, emailElem, 'img/mail.png');
    emailWin.attach(windowGroup, taskBar, desktop, windowList);
    windowList.push(emailWin);

    getEmail = (sender, subject, body) => {
        const newMessageClickable = document.createElement('div');
        newMessageClickable.innerHTML = `<div>${sender}</div><div>${subject}</div>`;
        emailWin.activate(true);
        newMessageClickable.classList.add('messageButton');
        newMessageClickable.onclick = () => {
            readerArea.innerHTML = '';
            const readerContent = document.createElement('div');
            readerContent.innerHTML = `<div>${sender}</div><div>${subject}</div><div>${body}</div>`;
            readerContent.classList.add('emailReader');
            readerArea.appendChild(readerContent)
        }
        messagesArea.appendChild(newMessageClickable)
        newMessageClickable.onclick()
    }
}

function unbelievableEngineUpdaterInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/uelogo.png'); background-size: cover; min-height: 10em; color: white"
    installer.innerHTML = `<h1>Installing Unbelievable Engine Updater...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        if (!conditions.commaWebR) {
            inner.innerHTML = 'Please install SmallFluffy CommaWeb Runtime version 3.0.4 to continue!'
        } else {
            try {
                rootdir.getChild('Programs').addChild(files.unbelievableEngineInstaller)
                inner.innerHTML = 'Successfully installed Unbelievable Engine Updater! Please run Programs/Unbelievable Engine Installer.exe to continue your installation process!'
            } catch (e) {
                inner.innerHTML = 'An error occurred when installing Unbelievable Engine Updater: '+e
            }
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`Unbelievable Engine Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

function unbelievableEngineInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/uelogo.png'); background-size: cover; min-height: 10em; color: white"
    installer.innerHTML = `<h1>Installing Unbelievable Engine...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        inner.innerHTML = `<div>
        Requirements: 
        <div>${conditions.commaWebD?'✅ Installed':'❌ Not Installed'} Comma Web Development Kit 6.0.3</div>
        <div>${conditions.auditiveGallery?'✅ Installed':'❌ Not Installed'} Auditive Gallery 5.x</div>
        <div>${conditions.registration?'✅ Installed':'❌ Not Installed'} Registration on Unbelievable Engine</div>
        </div>`
        if (!conditions.commaWebD || ! conditions.auditiveGallery || !conditions.doorsAPI || !conditions.registration) {
        } else {
            try {
                rootdir.getChild('Programs').addChild(files.unbelievableEngine)
                inner.innerHTML += 'Successfully installed Unbelievable Engine! Please run Programs/Unbelievable Engine.exe and enjoy!'
            } catch (e) {
                inner.innerHTML += 'An error occurred when installing Unbelievable Engine: '+e
            }
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`Unbelievable Engine Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

function commaWebInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/commaweb.png'); background-size: cover; min-height: 10em; color: white"
    installer.innerHTML = `<h1>Installing CommaWeb Development Kit...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        try {
            rootdir.getChild('Programs').addChild(files.commaWebDevkitInstalled)
            conditions.commaWebD = true
            inner.innerHTML = 'Successfully installed CommaWeb Development Kit!'
        } catch (e) {
            inner.innerHTML = 'An error occurred when installing CommaWeb Development Kit: '+e
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`CommaWeb Development Kit Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

function commaWebRuntimeInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/commaweb.png'); background-size: cover; min-height: 10em; color: white"
    installer.innerHTML = `<h1>Installing CommaWeb Runtime...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        try {
            rootdir.getChild('Programs').addChild(files.commaWebInstalled)
            conditions.commaWebR = true
            inner.innerHTML = 'Successfully installed CommaWeb Runtime!'
        } catch (e) {
            inner.innerHTML = 'An error occurred when installing CommaWeb Runtime: '+e
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`CommaWeb Runtime Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

function doorsAPIInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/doorsapi.png'); background-size: cover; min-height: 10em;"
    installer.innerHTML = `<h1>Installing Doors API...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        try {
            rootdir.getChild('Programs').addChild(files.doorsAPIInstalled)
            conditions.doorsAPI = true
            inner.innerHTML = 'Successfully installed Doors API!'
        } catch (e) {
            inner.innerHTML = 'An error occurred when installing Doors API: '+e
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`Doors API Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

function auditiveGalleryInstaller(rootdir) {
    let installer = document.createElement('div');
    installer.style = "background-image: url('img/auditivegallery.png'); background-size: cover; min-height: 10em; color: white"
    installer.innerHTML = `<h1>Installing Auditive Gallery...<h1>`
    setTimeout(() => {
        let inner = document.createElement('p');
        if (!conditions.doorsAPI) {
            inner.innerHTML = 'Please install Doors API to continue!'
        } else {
            try {
                rootdir.getChild('Programs').addChild(files.auditiveGalleryCommunityInstalled)
                conditions.auditiveGallery = true
                inner.innerHTML = 'Successfully installed Auditive Gallery!'
            } catch (e) {
                inner.innerHTML = 'An error occurred when installing Auditive Gallery: '+e
            }
        }
        installer.appendChild(inner)
    }, 2000)
    let installerWindow = new FOSWindow(`Auditive Gallery Installer`, {x:`30%`, y:`20em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, installer, 'img/web.png')
    installerWindow.closeButton.onclick = () => installerWindow.close(true)
    installerWindow.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(installerWindow)
    installerWindow.activate()
}

async function refreshDeletedList(){
    const files_deleted = {
        'Happy Cats Jumping Around.mp4': 9,
        'Game Network.exe': 2,
        'Desktop Pet.exe': 10,
        'Work Program Suite Pro.exe': 6,
        'Cursor changer.exe': 11,
        'Funny Meme.webm': 8,
        "Son's first birthday.mp4": 11,
        'Graduation party.mp4': 11,
        'Grandma.mp4': 12,
        'Puppy adoption.mp4': 10,
        'Family vacation.mp4': 14,
        'Wallpaper.png': 4,
        'Cherished memories': 6,
        'Simple moments': 10,
        'Happy times': 8,
        'Wedding photos': 6,
        'My Favourite Feel Good Playlist': 11,
        'Never being given up.mp3': 4,
        'Collection.zip': 13,
        'Moon photo.png': 12,
        'Funny gif.webm': 12,
        'Work files': 15,
        'OldFiles.zip': 13,
        'New Document.doc': 6,
        'Unbelievable Engine Updater Installer.exe': 10,
        'Recycle Bin': 5,
        Documents: 8,
        Downloads: 1,
        MyDocuments: 2,
        Pictures: 7,
        MyReceivedFiles: 1,
        Desktop: 2,
        Music: 7,
        Share: 2,
        Public: 2,
        Videos: 13,
        'New Document(1).doc': 5,
        'New Document(2).doc': 5,
        'New Document(3).doc': 5,
        'New Document(4).doc': 5,
        'New Document(5).doc': 5,
        'New Document(6).doc': 4,
        'New Document(7).doc': 4,
        'Quarterly Report (DRAFT).xls': 5,
        'Server Backup (old).tgz': 11,
        'Server Backup (copy).tgz': 10,
        'Server Backup.tgz': 7,
        'Doors API Installer.exe': 7,
        'Comma Web Runtime 2.x.exe': 17,
        'Comma Web Development Kit 4.2.3.exe': 6,
        'Comma Web Runtime 3.x.exe': 9,
        'Comma Web Development Kit 6.x.exe': 6,
        'Audit Documents (Do not delete!!!).zip': 4,
        'Auditive Gallery Community Edition Installer.exe': 8,
        'Quarterly Report.xls': 3,
        'Annual Report.xls': 3,
        'Staffing Schedule.xls': 3
    }

    const response = deletedFiles.map(k => ({k, v:((files_deleted[k]||0) / 26)}))
    console.log(response)
      
    document.getElementById("deletedFileList").innerHTML = response.sort((a,b) => a.v - b.v).map((e,i) => `<div style='grid-row: ${i+1}; grid-column: 1'>${e.k}</div><div style='grid-row: ${i+1}; grid-column: 2; z-index: 10'>${Math.ceil(e.v*100)}% of people also deleted this file</div><div style='background-color: #aaf; border: 1px solid #335; grid-row: ${i+1}; grid-column: 2; width:${Math.ceil(e.v*100)}%; height: 1em'></div>`).join('\n')
}

function winScreen(rootdir) {
    let congratulations = document.createElement('div');
    congratulations.innerHTML = `<h1>Congratulations! You installed Unbelievable Engine! But at what cost?</h1>
    <p>You deleted these files:</p>
    <div id="deletedFileList">
        ${deletedFiles.map(e => `<div style='grid-column: span 2'>${e}</div>`).join('\n')}
    </div>
    <p>Was it worth it?</p>
    <p>Special thanks to the 26 people who played and submitted their response before the servers were shut down!</p>`
    let winWin = new FOSWindow(`Congratulations...?`, {x:`100%`, y:`80%`}, {x:`0em`, y:`0em`}, congratulations, 'img/web.png')
    winWin.attach(windowGroup, taskBar, undefined, windowList);
    windowList.push(winWin)
    winWin.activate()
    refreshDeletedList()
}

let browser = document.createElement('div');

async function switchWebpage(page) {
    const response = await fetch(`websites/${page}.html`);
    browser.innerHTML = `<div class='browserHeader'>Bookmarks: <a href='javascript:switchWebpage("unbelieveableEngine")'>Unbelievable Engine Website</a></div> ${await response.text()}`;
}

function makeBrowser(rootdir) {
    browser.classList.add('Browser');
    switchWebpage('unbelieveableEngine');
    const browserWin = new FOSWindow(`Web`, {x:`40em`, y:`30em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, browser, 'img/web.png');
    browserWin.attach(windowGroup, taskBar, desktop, windowList);
    windowList.push(browserWin);
}
let rootdir;

const files = {
    unbelievableEngineUpdaterInstaller: new FOSFile('Unbelievable Engine Updater Installer.exe', 352000000),
    doorsAPI: new FOSFile('Doors API Installer.exe', 172000000),
    doorsAPIInstalled: new FOSFile('Doors API.exe', 4652200000),
    commaWebDevelopmentKit1: new FOSFile('Comma Web Development Kit 4.2.3.exe', 252000000),
    commaWebDevelopmentKit6: new FOSFile('Comma Web Development Kit 6.x.exe', 257000000),
    commaWebDevkitInstalled: new FOSFile('Comma Web Development Kit.exe', 26652200000),
    commaWebRuntime1: new FOSFile('Comma Web Runtime 2.x.exe', 183000000),
    commaWebRuntime3: new FOSFile('Comma Web Runtime 3.x.exe', 189000000),
    commaWebInstalled: new FOSFile('Comma Web Runtime.exe', 11652200000),
    auditiveGalleryCommunity: new FOSFile('Auditive Gallery Community Edition Installer.exe', 628000000),
    auditiveGalleryCommunityInstalled: new FOSFile('Auditive Gallery Community Edition.exe', 27522000000),
    unbelievableEngineInstaller: new FOSFile('Unbelievable Engine Installer.exe', 823000000),
    unbelievableEngine: new FOSFile('Unbelievable Engine.exe', 33652200000),
}

function download(file) {
    textPopup("Downloaded", `Downloaded ${file}`);
    rootdir.getChild('Downloads').addChild(files[file])
}

function init(){
    desktop = document.getElementById('desktop');
    taskBar = document.getElementById('taskbar');
    windowGroup = document.getElementById('windowGroup');
    let fileExplorer = document.createElement('div');
    rootdir = new Directory('/', undefined, [], 120000000000)
    rootdir.attach(true, fileExplorer);
    createDirStructure({dir: rootdir, c:[
        {dir: new Directory('Documents'), c:[
            {dir: new Directory('Work files'), c:[
                {f: new FOSFile("Server Backup.tgz", 12610000000)},
                {f: new FOSFile("Server Backup (copy).tgz", 12610000000)},
                {f: new FOSFile("Server Backup (old).tgz", 11470000000)},
                {f: new FOSFile("Quarterly Report.xls", 64920000)},
                {f: new FOSFile("Annual Report.xls", 164920000)},
                {f: new FOSFile("Staffing Schedule.xls", 49480000)},
                {f: new FOSFile("Audit Documents (Do not delete!!!).zip", 12071320000)},
            ]},
            {f: new FOSFile("OldFiles.zip", 2100000000)},
            {f: new FOSFile("New Document.doc", 13700000)},
            {f: new FOSFile("New Document(1).doc", 13700000)},
            {f: new FOSFile("New Document(2).doc", 7400000)},
            {f: new FOSFile("New Document(3).doc", 2900000)},
            {f: new FOSFile("New Document(4).doc", 20700000)},
            {f: new FOSFile("New Document(5).doc", 9300000)},
            {f: new FOSFile("New Document(6).doc", 7100000)},
            {f: new FOSFile("New Document(7).doc", 15900000)},
        ]},
        {dir: new Directory('Downloads'), c:[
            {f: new FOSFile("Collection.zip", 1260000000)},
            {f: new FOSFile("Moon photo.png", 64920000)},
            {f: new FOSFile("Funny gif.webm", 164920000)},
        ]},
        {dir: new Directory('MyDocuments'), c:[
            
        ]},
        {dir: new Directory('Recycle Bin'), c:[
            {f: new FOSFile("Quarterly Report (DRAFT).xls", 64320000)},
        ]},
        {dir: new Directory('Pictures'), c:[
            {f: new FOSFile("Wallpaper.png", 3410000)},
            {dir: new Directory('Cherished memories'), c:[
                {f:new FOSFile("IMG 0", 1934919.3185486475)},
                {f:new FOSFile("IMG 1", 5133247.812879165)},
                {f:new FOSFile("IMG 2", 1008101.0348055566)},
                {f:new FOSFile("IMG 3", 2072986.2484910942)},
                {f:new FOSFile("IMG 4", 2402024.968556803)},
                {f:new FOSFile("IMG 5", 2454803.1949612605)},
                {f:new FOSFile("IMG 6", 898562.4610395253)},
                {f:new FOSFile("IMG 7", 5371689.731813923)},
                {f:new FOSFile("IMG 8", 4294230.8514637835)},
                {f:new FOSFile("IMG 9", 654133.1227727582)},
                {f:new FOSFile("IMG 10", 1361417.3207920354)},
                {f:new FOSFile("IMG 11", 2629999.2695168583)},
                {f:new FOSFile("IMG 12", 1077088.5753495048)},
                {f:new FOSFile("IMG 13", 1659672.6488589246)},
                {f:new FOSFile("IMG 14", 1522475.336208689)},
                {f:new FOSFile("IMG 15", 4534093.631476997)},
                {f:new FOSFile("IMG 16", 5238003.332486153)},
                {f:new FOSFile("IMG 17", 1063165.231682828)},
                {f:new FOSFile("IMG 18", 1837560.9343681869)},
                {f:new FOSFile("IMG 19", 2710800.940491628)},
                {f:new FOSFile("IMG 20", 253869.23730138692)},
                {f:new FOSFile("IMG 21", 4147392.2875616)},
                {f:new FOSFile("IMG 22", 665962.0846395647)},
                {f:new FOSFile("IMG 23", 3610179.903317979)},
                {f:new FOSFile("IMG 24", 4764335.252428652)},
                {f:new FOSFile("IMG 25", 404009.8490326369)},
                {f:new FOSFile("IMG 26", 2908494.8510690858)},
                {f:new FOSFile("IMG 27", 532017.1194687218)},
                {f:new FOSFile("IMG 28", 3777642.651668614)},
                {f:new FOSFile("IMG 29", 1648237.8211357023)},
                {f:new FOSFile("IMG 30", 1459075.1039975923)},
                {f:new FOSFile("IMG 31", 4746418.200283798)},
                {f:new FOSFile("IMG 32", 1181318.4621195295)},
                {f:new FOSFile("IMG 33", 2265966.7191410395)},
                {f:new FOSFile("IMG 34", 5242177.712035598)},
                {f:new FOSFile("IMG 35", 1274433.7437142706)},
                {f:new FOSFile("IMG 36", 3060858.081460574)},
                {f:new FOSFile("IMG 37", 4751235.829884282)},
                {f:new FOSFile("IMG 38", 4639713.16357621)},
                {f:new FOSFile("IMG 39", 1716275.1497725218)},
                {f:new FOSFile("IMG 40", 3182255.462732055)},
                {f:new FOSFile("IMG 41", 4688028.038348409)},
                {f:new FOSFile("IMG 42", 2009636.9549468218)},
                {f:new FOSFile("IMG 43", 286043.0576994463)},
                {f:new FOSFile("IMG 44", 897.4055703632711)},
                {f:new FOSFile("IMG 45", 4697489.920198743)},
                {f:new FOSFile("IMG 46", 1943970.2893063598)},
                {f:new FOSFile("IMG 47", 879379.9791770682)},
                {f:new FOSFile("IMG 48", 3006720.4976409064)},
                {f:new FOSFile("IMG 49", 1829768.6649586346)},
                {f:new FOSFile("IMG 50", 3594117.1097387904)},
                {f:new FOSFile("IMG 51", 5121578.307297225)},
                {f:new FOSFile("IMG 52", 471997.2530050001)},
                {f:new FOSFile("IMG 53", 1143233.50420812)},
                {f:new FOSFile("IMG 54", 955420.7447746024)},
                {f:new FOSFile("IMG 55", 3549388.1211149315)},
                {f:new FOSFile("IMG 56", 1396118.0126096462)},
                {f:new FOSFile("IMG 57", 4183809.7054117504)},
                {f:new FOSFile("IMG 58", 2591172.946009412)},
                {f:new FOSFile("IMG 59", 379394.1233610112)},
                {f:new FOSFile("IMG 60", 4240243.312319416)},
                {f:new FOSFile("IMG 61", 4408218.272279176)},
                {f:new FOSFile("IMG 62", 899550.5227044325)},
                {f:new FOSFile("IMG 63", 2234626.990422657)},
                {f:new FOSFile("IMG 64", 511273.87477069616)},
                {f:new FOSFile("IMG 65", 4500530.775944228)},
                {f:new FOSFile("IMG 66", 5259142.744577927)},
                {f:new FOSFile("IMG 67", 4635228.019615685)},
                {f:new FOSFile("IMG 68", 505216.61085417884)},
                {f:new FOSFile("IMG 69", 3586008.491836827)},
                {f:new FOSFile("IMG 70", 2837203.147158156)},
                {f:new FOSFile("IMG 71", 3530468.993870205)},
                {f:new FOSFile("IMG 72", 2307651.9243988125)},
                {f:new FOSFile("IMG 73", 4699049.672451862)},
                {f:new FOSFile("IMG 74", 3975972.156606208)},
                {f:new FOSFile("IMG 75", 4276199.042703852)},
                {f:new FOSFile("IMG 76", 288624.3905550249)},
                {f:new FOSFile("IMG 77", 612547.1642024929)},
                {f:new FOSFile("IMG 78", 3665092.351154025)},
                {f:new FOSFile("IMG 79", 3087305.665705419)},
                {f:new FOSFile("IMG 80", 357576.6133348513)},
                {f:new FOSFile("IMG 81", 5219535.364552941)},
                {f:new FOSFile("IMG 82", 3288105.18948318)},
                {f:new FOSFile("IMG 83", 169208.29888639256)},
                {f:new FOSFile("IMG 84", 2418247.65398273)},
                {f:new FOSFile("IMG 85", 2703411.8814681866)},
                {f:new FOSFile("IMG 86", 5006039.632226185)},
                {f:new FOSFile("IMG 87", 4952407.956687038)},
                {f:new FOSFile("IMG 88", 3265748.2506593578)},
                {f:new FOSFile("IMG 89", 34591.79472938763)},
                {f:new FOSFile("IMG 90", 2494458.2530254857)},
                {f:new FOSFile("IMG 91", 1941900.2574907537)},
                {f:new FOSFile("IMG 92", 4357375.697969758)},
                {f:new FOSFile("IMG 93", 2434687.3631252656)},
                {f:new FOSFile("IMG 94", 939970.7754562793)},
                {f:new FOSFile("IMG 95", 4574222.85992188)},
                {f:new FOSFile("IMG 96", 2226496.391395752)},
                {f:new FOSFile("IMG 97", 1881663.1419013694)},
                {f:new FOSFile("IMG 98", 2424215.619399455)},
                {f:new FOSFile("IMG 99", 5334960.193946578)},
                {f:new FOSFile("IMG 100", 2361159.556342948)},
                {f:new FOSFile("IMG 101", 779174.1076243647)},
                {f:new FOSFile("IMG 102", 2578060.3179703425)},
                {f:new FOSFile("IMG 103", 4517080.886202562)},
                {f:new FOSFile("IMG 104", 4908332.66421883)},
                {f:new FOSFile("IMG 105", 2560423.316305693)},
                {f:new FOSFile("IMG 106", 67200.63325026212)},
                {f:new FOSFile("IMG 107", 2339160.9022653876)},
                {f:new FOSFile("IMG 108", 4516366.819674516)},
                {f:new FOSFile("IMG 109", 3451472.5755797117)},
                {f:new FOSFile("IMG 110", 4667765.592890652)},
                {f:new FOSFile("IMG 111", 2373377.1962752007)},
                {f:new FOSFile("IMG 112", 1838446.663891531)},
                {f:new FOSFile("IMG 113", 239049.87127649155)},
                {f:new FOSFile("IMG 114", 1043834.5517091961)},
                {f:new FOSFile("IMG 115", 1442201.1424371924)},
                {f:new FOSFile("IMG 116", 3055412.3290465223)},
                {f:new FOSFile("IMG 117", 567870.1223546645)},
                {f:new FOSFile("IMG 118", 1121368.9005260246)},
                {f:new FOSFile("IMG 119", 298039.9357625607)},
                {f:new FOSFile("IMG 120", 5134771.0563981915)},
                {f:new FOSFile("IMG 121", 49854.477389178326)},
                {f:new FOSFile("IMG 122", 362829.10695561016)},
                {f:new FOSFile("IMG 123", 758435.7770325354)},
                {f:new FOSFile("IMG 124", 2720020.6260516266)},
                {f:new FOSFile("IMG 125", 990342.1709389389)},
                {f:new FOSFile("IMG 126", 1219273.9075049246)},
                {f:new FOSFile("IMG 127", 1632491.313176052)},
                {f:new FOSFile("IMG 128", 474816.9593151865)},
                {f:new FOSFile("IMG 129", 2181518.613674591)},
                {f:new FOSFile("IMG 130", 2746727.4681797917)},
                {f:new FOSFile("IMG 131", 5330330.519871516)},
                {f:new FOSFile("IMG 132", 4888391.153408917)},
                {f:new FOSFile("IMG 133", 2694105.9720597416)},
                {f:new FOSFile("IMG 134", 5149567.748600311)},
                {f:new FOSFile("IMG 135", 2455963.8556068433)},
                {f:new FOSFile("IMG 136", 4796791.638360293)},
                {f:new FOSFile("IMG 137", 1501181.547619512)},
                {f:new FOSFile("IMG 138", 714690.483340779)},
                {f:new FOSFile("IMG 139", 4965502.242515027)},
                {f:new FOSFile("IMG 140", 4288481.032700917)},
                {f:new FOSFile("IMG 141", 5288749.717863177)},
                {f:new FOSFile("IMG 142", 4603858.426599511)},
                {f:new FOSFile("IMG 143", 3851792.3296487913)},
                {f:new FOSFile("IMG 144", 2281966.0203046505)},
                {f:new FOSFile("IMG 145", 3355475.6454589837)},
                {f:new FOSFile("IMG 146", 784605.0089771732)},
                {f:new FOSFile("IMG 147", 2739234.809680267)},
                {f:new FOSFile("IMG 148", 4342490.781365547)},
                {f:new FOSFile("IMG 149", 4965907.546086896)},
                {f:new FOSFile("IMG 150", 2954359.9797466076)},
                {f:new FOSFile("IMG 151", 3699749.067296748)},
                {f:new FOSFile("IMG 152", 1615572.333075509)},
                {f:new FOSFile("IMG 153", 807061.7789841506)},
                {f:new FOSFile("IMG 154", 518872.64930398064)},
                {f:new FOSFile("IMG 155", 1883763.311078725)},
                {f:new FOSFile("IMG 156", 481096.9128828525)},
                {f:new FOSFile("IMG 157", 3835873.551588227)},
                {f:new FOSFile("IMG 158", 756232.0483236323)},
                {f:new FOSFile("IMG 159", 255175.0296191889)},
                {f:new FOSFile("IMG 160", 187219.9216318475)},
                {f:new FOSFile("IMG 161", 4890274.135610149)},
                {f:new FOSFile("IMG 162", 1798838.096877109)},
                {f:new FOSFile("IMG 163", 1055683.711507954)},
                {f:new FOSFile("IMG 164", 1457225.4416372604)},
                {f:new FOSFile("IMG 165", 1716851.166815503)},
                {f:new FOSFile("IMG 166", 3189307.8731652605)},
                {f:new FOSFile("IMG 167", 4894811.176478292)},
                {f:new FOSFile("IMG 168", 3402608.401250744)},
                {f:new FOSFile("IMG 169", 2135362.473660635)},
                {f:new FOSFile("IMG 170", 272079.0782204053)},
                {f:new FOSFile("IMG 171", 939272.5061078134)},
                {f:new FOSFile("IMG 172", 4102522.0774454526)},
                {f:new FOSFile("IMG 173", 3224624.878779698)},
                {f:new FOSFile("IMG 174", 3754318.6658833455)},
                {f:new FOSFile("IMG 175", 4103071.1404134226)},
                {f:new FOSFile("IMG 176", 4290673.080648842)},
                {f:new FOSFile("IMG 177", 4057.1940869372256)},
                {f:new FOSFile("IMG 178", 5293728.106801933)},
                {f:new FOSFile("IMG 179", 5035882.531263538)},
                {f:new FOSFile("IMG 180", 3917169.208174236)},
                {f:new FOSFile("IMG 181", 4295425.631992236)},
                {f:new FOSFile("IMG 182", 3995864.8019931833)},
                {f:new FOSFile("IMG 183", 5265998.446541825)},
                {f:new FOSFile("IMG 184", 758258.712467804)},
                {f:new FOSFile("IMG 185", 1540026.8114017858)},
                {f:new FOSFile("IMG 186", 3885698.312085433)},
                {f:new FOSFile("IMG 187", 3097557.4722343427)},
                {f:new FOSFile("IMG 188", 2257157.4747311226)},
                {f:new FOSFile("IMG 189", 2061824.1709833439)},
                {f:new FOSFile("IMG 190", 3567124.970065588)},
                {f:new FOSFile("IMG 191", 1302702.4412102734)},
                {f:new FOSFile("IMG 192", 3211891.4072392667)},
                {f:new FOSFile("IMG 193", 2899673.072421647)},
                {f:new FOSFile("IMG 194", 578236.1192906979)},
                {f:new FOSFile("IMG 195", 2273181.634141097)},
                {f:new FOSFile("IMG 196", 2642407.4616994653)},
                {f:new FOSFile("IMG 197", 4139620.0299502024)},
                {f:new FOSFile("IMG 198", 3768311.8923414215)},
                {f:new FOSFile("IMG 199", 1270924.5214234209)},
                {f:new FOSFile("IMG 200", 4534157.118611752)},
                {f:new FOSFile("IMG 201", 2634492.6276640627)},
                {f:new FOSFile("IMG 202", 1507141.65386502)},
                {f:new FOSFile("IMG 203", 4922821.383554436)},
                {f:new FOSFile("IMG 204", 155525.72425945193)},
                {f:new FOSFile("IMG 205", 4630566.560456128)},
                {f:new FOSFile("IMG 206", 4696453.493523627)},
                {f:new FOSFile("IMG 207", 1840502.7587508543)},
                {f:new FOSFile("IMG 208", 2536796.6102995956)},
                {f:new FOSFile("IMG 209", 2647447.430692933)},
                {f:new FOSFile("IMG 210", 4874377.274202287)},
                {f:new FOSFile("IMG 211", 4841324.379367725)},
                {f:new FOSFile("IMG 212", 937350.0144322028)},
                {f:new FOSFile("IMG 213", 1995820.3850918484)},
                {f:new FOSFile("IMG 214", 944592.2607342725)},
                {f:new FOSFile("IMG 215", 1272093.688638466)},
                {f:new FOSFile("IMG 216", 4604959.153402186)},
                {f:new FOSFile("IMG 217", 3173251.2796821883)},
                {f:new FOSFile("IMG 218", 2819730.48113245)},
                {f:new FOSFile("IMG 219", 2368523.7784813507)},
                {f:new FOSFile("IMG 220", 22844.254421030608)},
                {f:new FOSFile("IMG 221", 85557.41095018301)},
                {f:new FOSFile("IMG 222", 1258790.6062454814)},
                {f:new FOSFile("IMG 223", 2931872.2289634687)},
                {f:new FOSFile("IMG 224", 3653516.922330765)},
                {f:new FOSFile("IMG 225", 5042370.3522961475)},
                {f:new FOSFile("IMG 226", 1805587.559072881)},
                {f:new FOSFile("IMG 227", 2019424.4972261947)},
                {f:new FOSFile("IMG 228", 145443.4046261982)},
                {f:new FOSFile("IMG 229", 2619213.471984463)},
                {f:new FOSFile("IMG 230", 3542501.815605475)},
                {f:new FOSFile("IMG 231", 1445869.8789149679)},
                {f:new FOSFile("IMG 232", 3432480.3976773014)},
                {f:new FOSFile("IMG 233", 2189801.0845361254)},
                {f:new FOSFile("IMG 234", 4580759.346139432)},
                {f:new FOSFile("IMG 235", 3552870.2151707965)},
                {f:new FOSFile("IMG 236", 807037.4079892025)},
                {f:new FOSFile("IMG 237", 2299604.3449412836)},
                {f:new FOSFile("IMG 238", 3565477.609649837)},
                {f:new FOSFile("IMG 239", 422737.61407940503)},
                {f:new FOSFile("IMG 240", 3374549.9964047186)},
                {f:new FOSFile("IMG 241", 4835865.6279133335)},
                {f:new FOSFile("IMG 242", 4895831.977444371)},
                {f:new FOSFile("IMG 243", 4699345.699876535)},
                {f:new FOSFile("IMG 244", 4227425.231217763)},
                {f:new FOSFile("IMG 245", 1488123.210629074)},
                {f:new FOSFile("IMG 246", 1221996.2900706865)},
                {f:new FOSFile("IMG 247", 682275.4850076572)},
                {f:new FOSFile("IMG 248", 3437817.8035750706)},
                {f:new FOSFile("IMG 249", 2870186.9138923124)},
                {f:new FOSFile("IMG 250", 1307514.4523984243)},
                {f:new FOSFile("IMG 251", 3128981.173612001)},
                {f:new FOSFile("IMG 252", 2891652.2865193738)},
                {f:new FOSFile("IMG 253", 3052166.9699399127)},
                {f:new FOSFile("IMG 254", 4129212.045449729)},
                {f:new FOSFile("IMG 255", 2330675.1527489745)},
                {f:new FOSFile("IMG 256", 4376661.104967378)},
                {f:new FOSFile("IMG 257", 4376590.439626714)},
                {f:new FOSFile("IMG 258", 262061.92486415588)},
                {f:new FOSFile("IMG 259", 4147639.5186395105)},
                {f:new FOSFile("IMG 260", 604724.7546587375)},
                {f:new FOSFile("IMG 261", 1449762.720079222)},
                {f:new FOSFile("IMG 262", 2402034.2049776115)},
                {f:new FOSFile("IMG 263", 5141673.329675995)},
                {f:new FOSFile("IMG 264", 2273770.612797021)},
                {f:new FOSFile("IMG 265", 5217748.16109118)},
                {f:new FOSFile("IMG 266", 4195629.631197083)},
                {f:new FOSFile("IMG 267", 226636.14709865584)},
                {f:new FOSFile("IMG 268", 249338.55771680974)},
                {f:new FOSFile("IMG 269", 3877567.3009855533)},
                {f:new FOSFile("IMG 270", 3617556.3931955216)},
                {f:new FOSFile("IMG 271", 762665.1461218365)},
                {f:new FOSFile("IMG 272", 2677585.7783309086)},
                {f:new FOSFile("IMG 273", 1241034.3201902467)},
                {f:new FOSFile("IMG 274", 438569.5011076022)},
                {f:new FOSFile("IMG 275", 3808169.3322413284)},
                {f:new FOSFile("IMG 276", 1630060.1436121908)},
                {f:new FOSFile("IMG 277", 4719146.402712899)},
                {f:new FOSFile("IMG 278", 2037235.7998763036)},
                {f:new FOSFile("IMG 279", 3903219.291955159)},
                {f:new FOSFile("IMG 280", 3981269.456213362)},
                {f:new FOSFile("IMG 281", 4187083.8911796175)},
                {f:new FOSFile("IMG 282", 997158.5978612704)},
                {f:new FOSFile("IMG 283", 5080903.989654513)},
                {f:new FOSFile("IMG 284", 5147124.59350075)},
                {f:new FOSFile("IMG 285", 4747669.222484612)},
                {f:new FOSFile("IMG 286", 430761.3342404406)},
                {f:new FOSFile("IMG 287", 1318778.0133932189)},
                {f:new FOSFile("IMG 288", 691637.1722308178)},
                {f:new FOSFile("IMG 289", 408743.4787058119)},
                {f:new FOSFile("IMG 290", 4555179.196667865)},
                {f:new FOSFile("IMG 291", 3295025.064585859)},
                {f:new FOSFile("IMG 292", 2038505.748604515)},
                {f:new FOSFile("IMG 293", 5182234.807131301)},
                {f:new FOSFile("IMG 294", 3537397.447906229)},
                {f:new FOSFile("IMG 295", 1894634.9914578118)},
                {f:new FOSFile("IMG 296", 5106520.218848962)},
                {f:new FOSFile("IMG 297", 1487961.9517260736)},
                {f:new FOSFile("IMG 298", 1846771.6311247987)},
                {f:new FOSFile("IMG 299", 4881810.069995194)},
                {f:new FOSFile("IMG 300", 3120349.0325842244)},
                {f:new FOSFile("IMG 301", 4634517.048924282)},
                {f:new FOSFile("IMG 302", 2214861.034280466)},
                {f:new FOSFile("IMG 303", 2547290.1816923013)},
                {f:new FOSFile("IMG 304", 277213.3128142533)},
                {f:new FOSFile("IMG 305", 3003654.2650011256)},
                {f:new FOSFile("IMG 306", 3361907.8413597215)},
                {f:new FOSFile("IMG 307", 4729212.013640853)},
                {f:new FOSFile("IMG 308", 2520379.023308253)},
                {f:new FOSFile("IMG 309", 510034.66591885703)},
                {f:new FOSFile("IMG 310", 1780582.628670466)},
                {f:new FOSFile("IMG 311", 5364639.721155697)},
                {f:new FOSFile("IMG 312", 3560246.580668569)},
                {f:new FOSFile("IMG 313", 899603.6817454125)},
                {f:new FOSFile("IMG 314", 5355663.844603287)},
                {f:new FOSFile("IMG 315", 1573723.8929499874)},
                {f:new FOSFile("IMG 316", 235261.75446038696)},
                {f:new FOSFile("IMG 317", 3418979.0014966535)},
                {f:new FOSFile("IMG 318", 2530892.6922622775)},
                {f:new FOSFile("IMG 319", 1142738.463025958)},
                {f:new FOSFile("IMG 320", 727667.2951079941)},
                {f:new FOSFile("IMG 321", 326180.18196474813)},
                {f:new FOSFile("IMG 322", 3136599.1740820464)},
                {f:new FOSFile("IMG 323", 5135345.787680329)},
                {f:new FOSFile("IMG 324", 355581.8628757596)},
                {f:new FOSFile("IMG 325", 322995.08441116614)},
                {f:new FOSFile("IMG 326", 4523057.531342793)},
                {f:new FOSFile("IMG 327", 4549185.857551276)},
                {f:new FOSFile("IMG 328", 1629461.4060904558)},
                {f:new FOSFile("IMG 329", 4072015.517662863)},
                {f:new FOSFile("IMG 330", 3687591.9056853275)},
                {f:new FOSFile("IMG 331", 3099033.7436816925)},
                {f:new FOSFile("IMG 332", 4388147.721035349)},
                {f:new FOSFile("IMG 333", 4495508.040653465)},
                {f:new FOSFile("IMG 334", 5079764.316499512)},
                {f:new FOSFile("IMG 335", 708499.4003566242)},
                {f:new FOSFile("IMG 336", 1238027.4479726206)},
                {f:new FOSFile("IMG 337", 3069178.3640650907)},
                {f:new FOSFile("IMG 338", 2997369.3361659287)},
                {f:new FOSFile("IMG 339", 2319712.681092647)},
                {f:new FOSFile("IMG 340", 4469925.192671735)},
                {f:new FOSFile("IMG 341", 3864726.7962367157)},
                {f:new FOSFile("IMG 342", 4784797.796183844)},
                {f:new FOSFile("IMG 343", 702633.1622921046)},
            ]},
            {dir: new Directory('Simple moments'), c:[
                {f:new FOSFile("IMG 0", 1934919.3185486475)},
                {f:new FOSFile("IMG 1", 5133247.812879165)},
                {f:new FOSFile("IMG 2", 1008101.0348055566)},
                {f:new FOSFile("IMG 3", 2072986.2484910942)},
                {f:new FOSFile("IMG 4", 2402024.968556803)},
                {f:new FOSFile("IMG 5", 2454803.1949612605)},
                {f:new FOSFile("IMG 6", 898562.4610395253)},
                {f:new FOSFile("IMG 7", 5371689.731813923)},
                {f:new FOSFile("IMG 8", 4294230.8514637835)},
                {f:new FOSFile("IMG 9", 654133.1227727582)},
                {f:new FOSFile("IMG 10", 1361417.3207920354)},
                {f:new FOSFile("IMG 11", 2629999.2695168583)},
                {f:new FOSFile("IMG 12", 1077088.5753495048)},
                {f:new FOSFile("IMG 13", 1659672.6488589246)},
                {f:new FOSFile("IMG 14", 1522475.336208689)},
                {f:new FOSFile("IMG 15", 4534093.631476997)},
                {f:new FOSFile("IMG 16", 5238003.332486153)},
                {f:new FOSFile("IMG 17", 1063165.231682828)},
                {f:new FOSFile("IMG 18", 1837560.9343681869)},
                {f:new FOSFile("IMG 19", 2710800.940491628)},
                {f:new FOSFile("IMG 20", 253869.23730138692)},
                {f:new FOSFile("IMG 21", 4147392.2875616)},
                {f:new FOSFile("IMG 22", 665962.0846395647)},
                {f:new FOSFile("IMG 23", 3610179.903317979)},
                {f:new FOSFile("IMG 24", 4764335.252428652)},
                {f:new FOSFile("IMG 25", 404009.8490326369)},
                {f:new FOSFile("IMG 26", 2908494.8510690858)},
                {f:new FOSFile("IMG 27", 532017.1194687218)},
                {f:new FOSFile("IMG 28", 3777642.651668614)},
                {f:new FOSFile("IMG 29", 1648237.8211357023)},
                {f:new FOSFile("IMG 30", 1459075.1039975923)},
                {f:new FOSFile("IMG 31", 4746418.200283798)},
                {f:new FOSFile("IMG 32", 1181318.4621195295)},
                {f:new FOSFile("IMG 33", 2265966.7191410395)},
                {f:new FOSFile("IMG 34", 5242177.712035598)},
                {f:new FOSFile("IMG 35", 1274433.7437142706)},
                {f:new FOSFile("IMG 36", 3060858.081460574)},
                {f:new FOSFile("IMG 37", 4751235.829884282)},
                {f:new FOSFile("IMG 38", 4639713.16357621)},
                {f:new FOSFile("IMG 39", 1716275.1497725218)},
                {f:new FOSFile("IMG 40", 3182255.462732055)},
                {f:new FOSFile("IMG 41", 4688028.038348409)},
                {f:new FOSFile("IMG 42", 2009636.9549468218)},
                {f:new FOSFile("IMG 43", 286043.0576994463)},
                {f:new FOSFile("IMG 44", 897.4055703632711)},
                {f:new FOSFile("IMG 45", 4697489.920198743)},
                {f:new FOSFile("IMG 46", 1943970.2893063598)},
                {f:new FOSFile("IMG 47", 879379.9791770682)},
                {f:new FOSFile("IMG 48", 3006720.4976409064)},
                {f:new FOSFile("IMG 49", 1829768.6649586346)},
                {f:new FOSFile("IMG 50", 3594117.1097387904)},
                {f:new FOSFile("IMG 51", 5121578.307297225)},
                {f:new FOSFile("IMG 52", 471997.2530050001)},
                {f:new FOSFile("IMG 53", 1143233.50420812)},
                {f:new FOSFile("IMG 54", 955420.7447746024)},
                {f:new FOSFile("IMG 55", 3549388.1211149315)},
                {f:new FOSFile("IMG 56", 1396118.0126096462)},
                {f:new FOSFile("IMG 57", 4183809.7054117504)},
                {f:new FOSFile("IMG 58", 2591172.946009412)},
                {f:new FOSFile("IMG 59", 379394.1233610112)},
                {f:new FOSFile("IMG 60", 4240243.312319416)},
                {f:new FOSFile("IMG 61", 4408218.272279176)},
                {f:new FOSFile("IMG 62", 899550.5227044325)},
                {f:new FOSFile("IMG 63", 2234626.990422657)},
                {f:new FOSFile("IMG 64", 511273.87477069616)},
                {f:new FOSFile("IMG 65", 4500530.775944228)},
                {f:new FOSFile("IMG 66", 5259142.744577927)},
                {f:new FOSFile("IMG 67", 4635228.019615685)},
                {f:new FOSFile("IMG 68", 505216.61085417884)},
                {f:new FOSFile("IMG 69", 3586008.491836827)},
                {f:new FOSFile("IMG 70", 2837203.147158156)},
                {f:new FOSFile("IMG 71", 3530468.993870205)},
                {f:new FOSFile("IMG 72", 2307651.9243988125)},
                {f:new FOSFile("IMG 73", 4699049.672451862)},
                {f:new FOSFile("IMG 74", 3975972.156606208)},
                {f:new FOSFile("IMG 75", 4276199.042703852)},
                {f:new FOSFile("IMG 76", 288624.3905550249)},
                {f:new FOSFile("IMG 77", 612547.1642024929)},
                {f:new FOSFile("IMG 78", 3665092.351154025)},
                {f:new FOSFile("IMG 79", 3087305.665705419)},
                {f:new FOSFile("IMG 80", 357576.6133348513)},
                {f:new FOSFile("IMG 81", 5219535.364552941)},
                {f:new FOSFile("IMG 82", 3288105.18948318)},
                {f:new FOSFile("IMG 83", 169208.29888639256)},
                {f:new FOSFile("IMG 84", 2418247.65398273)},
                {f:new FOSFile("IMG 85", 2703411.8814681866)},
                {f:new FOSFile("IMG 86", 5006039.632226185)},
                {f:new FOSFile("IMG 87", 4952407.956687038)},
                {f:new FOSFile("IMG 88", 3265748.2506593578)},
                {f:new FOSFile("IMG 89", 34591.79472938763)},
                {f:new FOSFile("IMG 90", 2494458.2530254857)},
                {f:new FOSFile("IMG 91", 1941900.2574907537)},
                {f:new FOSFile("IMG 92", 4357375.697969758)},
                {f:new FOSFile("IMG 93", 2434687.3631252656)},
                {f:new FOSFile("IMG 94", 939970.7754562793)},
                {f:new FOSFile("IMG 95", 4574222.85992188)},
                {f:new FOSFile("IMG 96", 2226496.391395752)},
                {f:new FOSFile("IMG 97", 1881663.1419013694)},
                {f:new FOSFile("IMG 98", 2424215.619399455)},
                {f:new FOSFile("IMG 99", 5334960.193946578)},
                {f:new FOSFile("IMG 100", 2361159.556342948)},
                {f:new FOSFile("IMG 101", 779174.1076243647)},
                {f:new FOSFile("IMG 102", 2578060.3179703425)},
                {f:new FOSFile("IMG 103", 4517080.886202562)},
                {f:new FOSFile("IMG 104", 4908332.66421883)},
                {f:new FOSFile("IMG 105", 2560423.316305693)},
                {f:new FOSFile("IMG 106", 67200.63325026212)},
                {f:new FOSFile("IMG 107", 2339160.9022653876)},
                {f:new FOSFile("IMG 108", 4516366.819674516)},
                {f:new FOSFile("IMG 109", 3451472.5755797117)},
                {f:new FOSFile("IMG 110", 4667765.592890652)},
                {f:new FOSFile("IMG 111", 2373377.1962752007)},
                {f:new FOSFile("IMG 112", 1838446.663891531)},
                {f:new FOSFile("IMG 113", 239049.87127649155)},
                {f:new FOSFile("IMG 114", 1043834.5517091961)},
                {f:new FOSFile("IMG 115", 1442201.1424371924)},
                {f:new FOSFile("IMG 116", 3055412.3290465223)},
                {f:new FOSFile("IMG 117", 567870.1223546645)},
                {f:new FOSFile("IMG 118", 1121368.9005260246)},
                {f:new FOSFile("IMG 119", 298039.9357625607)},
                {f:new FOSFile("IMG 120", 5134771.0563981915)},
                {f:new FOSFile("IMG 121", 49854.477389178326)},
                {f:new FOSFile("IMG 122", 362829.10695561016)},
                {f:new FOSFile("IMG 123", 758435.7770325354)},
                {f:new FOSFile("IMG 124", 2720020.6260516266)},
                {f:new FOSFile("IMG 125", 990342.1709389389)},
                {f:new FOSFile("IMG 126", 1219273.9075049246)},
                {f:new FOSFile("IMG 127", 1632491.313176052)},
                {f:new FOSFile("IMG 128", 474816.9593151865)},
                {f:new FOSFile("IMG 129", 2181518.613674591)},
                {f:new FOSFile("IMG 130", 2746727.4681797917)},
                {f:new FOSFile("IMG 131", 5330330.519871516)},
                {f:new FOSFile("IMG 132", 4888391.153408917)},
                {f:new FOSFile("IMG 133", 2694105.9720597416)},
                {f:new FOSFile("IMG 134", 5149567.748600311)},
                {f:new FOSFile("IMG 135", 2455963.8556068433)},
                {f:new FOSFile("IMG 136", 4796791.638360293)},
                {f:new FOSFile("IMG 137", 1501181.547619512)},
                {f:new FOSFile("IMG 138", 714690.483340779)},
                {f:new FOSFile("IMG 139", 4965502.242515027)},
                {f:new FOSFile("IMG 140", 4288481.032700917)},
                {f:new FOSFile("IMG 141", 5288749.717863177)},
                {f:new FOSFile("IMG 142", 4603858.426599511)},
                {f:new FOSFile("IMG 143", 3851792.3296487913)},
                {f:new FOSFile("IMG 144", 2281966.0203046505)},
                {f:new FOSFile("IMG 145", 3355475.6454589837)},
                {f:new FOSFile("IMG 146", 784605.0089771732)},
                {f:new FOSFile("IMG 147", 2739234.809680267)},
                {f:new FOSFile("IMG 148", 4342490.781365547)},
                {f:new FOSFile("IMG 149", 4965907.546086896)},
                {f:new FOSFile("IMG 150", 2954359.9797466076)},
                {f:new FOSFile("IMG 151", 3699749.067296748)},
                {f:new FOSFile("IMG 152", 1615572.333075509)},
                {f:new FOSFile("IMG 153", 807061.7789841506)},
                {f:new FOSFile("IMG 154", 518872.64930398064)},
                {f:new FOSFile("IMG 155", 1883763.311078725)},
                {f:new FOSFile("IMG 156", 481096.9128828525)},
                {f:new FOSFile("IMG 157", 3835873.551588227)},
                {f:new FOSFile("IMG 158", 756232.0483236323)},
                {f:new FOSFile("IMG 159", 255175.0296191889)},
                {f:new FOSFile("IMG 160", 187219.9216318475)},
                {f:new FOSFile("IMG 161", 4890274.135610149)},
                {f:new FOSFile("IMG 162", 1798838.096877109)},
                {f:new FOSFile("IMG 163", 1055683.711507954)},
                {f:new FOSFile("IMG 164", 1457225.4416372604)},
                {f:new FOSFile("IMG 165", 1716851.166815503)},
                {f:new FOSFile("IMG 166", 3189307.8731652605)},
                {f:new FOSFile("IMG 167", 4894811.176478292)},
                {f:new FOSFile("IMG 168", 3402608.401250744)},
                {f:new FOSFile("IMG 169", 2135362.473660635)},
                {f:new FOSFile("IMG 170", 272079.0782204053)},
                {f:new FOSFile("IMG 171", 939272.5061078134)},
                {f:new FOSFile("IMG 172", 4102522.0774454526)},
                {f:new FOSFile("IMG 173", 3224624.878779698)},
                {f:new FOSFile("IMG 174", 3754318.6658833455)},
                {f:new FOSFile("IMG 175", 4103071.1404134226)},
                {f:new FOSFile("IMG 176", 4290673.080648842)},
                {f:new FOSFile("IMG 177", 4057.1940869372256)},
                {f:new FOSFile("IMG 178", 5293728.106801933)},
                {f:new FOSFile("IMG 179", 5035882.531263538)},
                {f:new FOSFile("IMG 180", 3917169.208174236)},
                {f:new FOSFile("IMG 181", 4295425.631992236)},
                {f:new FOSFile("IMG 182", 3995864.8019931833)},
                {f:new FOSFile("IMG 183", 5265998.446541825)},
                {f:new FOSFile("IMG 184", 758258.712467804)},
                {f:new FOSFile("IMG 185", 1540026.8114017858)},
                {f:new FOSFile("IMG 186", 3885698.312085433)},
                {f:new FOSFile("IMG 187", 3097557.4722343427)},
                {f:new FOSFile("IMG 188", 2257157.4747311226)},
                {f:new FOSFile("IMG 189", 2061824.1709833439)},
                {f:new FOSFile("IMG 190", 3567124.970065588)},
                {f:new FOSFile("IMG 191", 1302702.4412102734)},
                {f:new FOSFile("IMG 192", 3211891.4072392667)},
                {f:new FOSFile("IMG 193", 2899673.072421647)},
                {f:new FOSFile("IMG 194", 578236.1192906979)},
                {f:new FOSFile("IMG 195", 2273181.634141097)},
                {f:new FOSFile("IMG 196", 2642407.4616994653)},
                {f:new FOSFile("IMG 197", 4139620.0299502024)},
                {f:new FOSFile("IMG 198", 3768311.8923414215)},
                {f:new FOSFile("IMG 199", 1270924.5214234209)},
                {f:new FOSFile("IMG 200", 4534157.118611752)},
                {f:new FOSFile("IMG 201", 2634492.6276640627)},
                {f:new FOSFile("IMG 202", 1507141.65386502)},
                {f:new FOSFile("IMG 203", 4922821.383554436)},
                {f:new FOSFile("IMG 204", 155525.72425945193)},
                {f:new FOSFile("IMG 205", 4630566.560456128)},
                {f:new FOSFile("IMG 206", 4696453.493523627)},
                {f:new FOSFile("IMG 207", 1840502.7587508543)},
                {f:new FOSFile("IMG 208", 2536796.6102995956)},
                {f:new FOSFile("IMG 209", 2647447.430692933)},
                {f:new FOSFile("IMG 210", 4874377.274202287)},
                {f:new FOSFile("IMG 211", 4841324.379367725)},
                {f:new FOSFile("IMG 212", 937350.0144322028)},
                {f:new FOSFile("IMG 213", 1995820.3850918484)},
                {f:new FOSFile("IMG 214", 944592.2607342725)},
                {f:new FOSFile("IMG 215", 1272093.688638466)},
                {f:new FOSFile("IMG 216", 4604959.153402186)},
                {f:new FOSFile("IMG 217", 3173251.2796821883)},
                {f:new FOSFile("IMG 218", 2819730.48113245)},
                {f:new FOSFile("IMG 219", 2368523.7784813507)},
                {f:new FOSFile("IMG 220", 22844.254421030608)},
                {f:new FOSFile("IMG 221", 85557.41095018301)},
                {f:new FOSFile("IMG 222", 1258790.6062454814)},
                {f:new FOSFile("IMG 223", 2931872.2289634687)},
                {f:new FOSFile("IMG 224", 3653516.922330765)},
                {f:new FOSFile("IMG 225", 5042370.3522961475)},
                {f:new FOSFile("IMG 226", 1805587.559072881)},
                {f:new FOSFile("IMG 227", 2019424.4972261947)},
                {f:new FOSFile("IMG 228", 145443.4046261982)},
                {f:new FOSFile("IMG 229", 2619213.471984463)},
                {f:new FOSFile("IMG 230", 3542501.815605475)},
                {f:new FOSFile("IMG 231", 1445869.8789149679)},
                {f:new FOSFile("IMG 232", 3432480.3976773014)},
                {f:new FOSFile("IMG 233", 2189801.0845361254)},
                {f:new FOSFile("IMG 234", 4580759.346139432)},
                {f:new FOSFile("IMG 235", 3552870.2151707965)},
                {f:new FOSFile("IMG 236", 807037.4079892025)},
                {f:new FOSFile("IMG 237", 2299604.3449412836)},
                {f:new FOSFile("IMG 238", 3565477.609649837)},
                {f:new FOSFile("IMG 239", 422737.61407940503)},
                {f:new FOSFile("IMG 240", 3374549.9964047186)},
                {f:new FOSFile("IMG 241", 4835865.6279133335)},
                {f:new FOSFile("IMG 242", 4895831.977444371)},
                {f:new FOSFile("IMG 243", 4699345.699876535)},
                {f:new FOSFile("IMG 244", 4227425.231217763)},
                {f:new FOSFile("IMG 245", 1488123.210629074)},
                {f:new FOSFile("IMG 246", 1221996.2900706865)},
                {f:new FOSFile("IMG 247", 682275.4850076572)},
                {f:new FOSFile("IMG 248", 3437817.8035750706)},
                {f:new FOSFile("IMG 249", 2870186.9138923124)},
                {f:new FOSFile("IMG 250", 1307514.4523984243)},
                {f:new FOSFile("IMG 251", 3128981.173612001)},
                {f:new FOSFile("IMG 252", 2891652.2865193738)},
                {f:new FOSFile("IMG 253", 3052166.9699399127)},
                {f:new FOSFile("IMG 254", 4129212.045449729)},
                {f:new FOSFile("IMG 255", 2330675.1527489745)},
                {f:new FOSFile("IMG 256", 4376661.104967378)},
                {f:new FOSFile("IMG 257", 4376590.439626714)},
                {f:new FOSFile("IMG 258", 262061.92486415588)},
                {f:new FOSFile("IMG 259", 4147639.5186395105)},
                {f:new FOSFile("IMG 260", 604724.7546587375)},
                {f:new FOSFile("IMG 261", 1449762.720079222)},
                {f:new FOSFile("IMG 262", 2402034.2049776115)},
                {f:new FOSFile("IMG 263", 5141673.329675995)},
                {f:new FOSFile("IMG 264", 2273770.612797021)},
                {f:new FOSFile("IMG 265", 5217748.16109118)},
                {f:new FOSFile("IMG 266", 4195629.631197083)},
                {f:new FOSFile("IMG 267", 226636.14709865584)},
                {f:new FOSFile("IMG 268", 249338.55771680974)},
                {f:new FOSFile("IMG 269", 3877567.3009855533)},
                {f:new FOSFile("IMG 270", 3617556.3931955216)},
                {f:new FOSFile("IMG 271", 762665.1461218365)},
                {f:new FOSFile("IMG 272", 2677585.7783309086)},
                {f:new FOSFile("IMG 273", 1241034.3201902467)},
                {f:new FOSFile("IMG 274", 438569.5011076022)},
                {f:new FOSFile("IMG 275", 3808169.3322413284)},
                {f:new FOSFile("IMG 276", 1630060.1436121908)},
                {f:new FOSFile("IMG 277", 4719146.402712899)},
                {f:new FOSFile("IMG 278", 2037235.7998763036)},
                {f:new FOSFile("IMG 279", 3903219.291955159)},
                {f:new FOSFile("IMG 280", 3981269.456213362)},
                {f:new FOSFile("IMG 281", 4187083.8911796175)},
                {f:new FOSFile("IMG 282", 997158.5978612704)},
                {f:new FOSFile("IMG 283", 5080903.989654513)},
                {f:new FOSFile("IMG 284", 5147124.59350075)},
                {f:new FOSFile("IMG 285", 4747669.222484612)},
                {f:new FOSFile("IMG 286", 430761.3342404406)},
                {f:new FOSFile("IMG 287", 1318778.0133932189)},
                {f:new FOSFile("IMG 288", 691637.1722308178)},
                {f:new FOSFile("IMG 289", 408743.4787058119)},
                {f:new FOSFile("IMG 290", 4555179.196667865)},
                {f:new FOSFile("IMG 291", 3295025.064585859)},
                {f:new FOSFile("IMG 292", 2038505.748604515)},
                {f:new FOSFile("IMG 293", 5182234.807131301)},
                {f:new FOSFile("IMG 294", 3537397.447906229)},
                {f:new FOSFile("IMG 295", 1894634.9914578118)},
                {f:new FOSFile("IMG 296", 5106520.218848962)},
                {f:new FOSFile("IMG 297", 1487961.9517260736)},
                {f:new FOSFile("IMG 298", 1846771.6311247987)},
                {f:new FOSFile("IMG 299", 4881810.069995194)},
                {f:new FOSFile("IMG 300", 3120349.0325842244)},
                {f:new FOSFile("IMG 301", 4634517.048924282)},
                {f:new FOSFile("IMG 302", 2214861.034280466)},
                {f:new FOSFile("IMG 303", 2547290.1816923013)},
                {f:new FOSFile("IMG 304", 277213.3128142533)},
                {f:new FOSFile("IMG 305", 3003654.2650011256)},
                {f:new FOSFile("IMG 306", 3361907.8413597215)},
                {f:new FOSFile("IMG 307", 4729212.013640853)},
                {f:new FOSFile("IMG 308", 2520379.023308253)},
                {f:new FOSFile("IMG 309", 510034.66591885703)},
                {f:new FOSFile("IMG 310", 1780582.628670466)},
                {f:new FOSFile("IMG 311", 5364639.721155697)},
                {f:new FOSFile("IMG 312", 3560246.580668569)},
                {f:new FOSFile("IMG 313", 899603.6817454125)},
                {f:new FOSFile("IMG 314", 5355663.844603287)},
                {f:new FOSFile("IMG 315", 1573723.8929499874)},
                {f:new FOSFile("IMG 316", 235261.75446038696)},
                {f:new FOSFile("IMG 317", 3418979.0014966535)},
                {f:new FOSFile("IMG 318", 2530892.6922622775)},
                {f:new FOSFile("IMG 319", 1142738.463025958)},
                {f:new FOSFile("IMG 320", 727667.2951079941)},
                {f:new FOSFile("IMG 321", 326180.18196474813)},
                {f:new FOSFile("IMG 322", 3136599.1740820464)},
                {f:new FOSFile("IMG 323", 5135345.787680329)},
                {f:new FOSFile("IMG 324", 355581.8628757596)},
                {f:new FOSFile("IMG 325", 322995.08441116614)},
                {f:new FOSFile("IMG 326", 4523057.531342793)},
                {f:new FOSFile("IMG 327", 4549185.857551276)},
                {f:new FOSFile("IMG 328", 1629461.4060904558)},
                {f:new FOSFile("IMG 329", 4072015.517662863)},
                {f:new FOSFile("IMG 330", 3687591.9056853275)},
                {f:new FOSFile("IMG 331", 3099033.7436816925)},
                {f:new FOSFile("IMG 332", 4388147.721035349)},
                {f:new FOSFile("IMG 333", 4495508.040653465)},
                {f:new FOSFile("IMG 334", 5079764.316499512)},
                {f:new FOSFile("IMG 335", 708499.4003566242)},
                {f:new FOSFile("IMG 336", 1238027.4479726206)},
                {f:new FOSFile("IMG 337", 3069178.3640650907)},
                {f:new FOSFile("IMG 338", 2997369.3361659287)},
                {f:new FOSFile("IMG 339", 2319712.681092647)},
                {f:new FOSFile("IMG 340", 4469925.192671735)},
                {f:new FOSFile("IMG 341", 3864726.7962367157)},
                {f:new FOSFile("IMG 342", 4784797.796183844)},
                {f:new FOSFile("IMG 343", 702633.1622921046)},
                {f:new FOSFile("IMG 344", 4437008.587379745)},
                {f:new FOSFile("IMG 345", 49759.37837611794)},
                {f:new FOSFile("IMG 346", 4846680.953851022)},
                {f:new FOSFile("IMG 347", 3018179.593939715)},
                {f:new FOSFile("IMG 348", 4079411.408572488)},
                {f:new FOSFile("IMG 349", 2237870.9380910466)},
                {f:new FOSFile("IMG 350", 2296036.735122648)},
                {f:new FOSFile("IMG 351", 2241771.982190919)},
                {f:new FOSFile("IMG 352", 5288384.969391077)},
                {f:new FOSFile("IMG 353", 7439.497937376621)},
                {f:new FOSFile("IMG 354", 1253636.599192399)},
                {f:new FOSFile("IMG 355", 4495136.97691003)},
                {f:new FOSFile("IMG 356", 1048756.8298670792)},
                {f:new FOSFile("IMG 357", 190452.85494589293)},
                {f:new FOSFile("IMG 358", 2333836.4446285795)},
                {f:new FOSFile("IMG 359", 3589879.1649703123)},
                {f:new FOSFile("IMG 360", 5389940.214379923)},
                {f:new FOSFile("IMG 361", 1427792.272798268)},
                {f:new FOSFile("IMG 362", 2416284.1941786166)},
                {f:new FOSFile("IMG 363", 2867390.638178655)},
                {f:new FOSFile("IMG 364", 2063707.593226942)},
                {f:new FOSFile("IMG 365", 1752768.8975842204)},
                {f:new FOSFile("IMG 366", 563464.0080675418)},
                {f:new FOSFile("IMG 367", 273394.34471659985)},
                {f:new FOSFile("IMG 368", 5372247.187301178)},
                {f:new FOSFile("IMG 369", 3495526.980899779)},
                {f:new FOSFile("IMG 370", 502777.1012791989)},
                {f:new FOSFile("IMG 371", 2177220.851217214)},
                {f:new FOSFile("IMG 372", 5081422.533982912)},
                {f:new FOSFile("IMG 373", 2278916.954374587)},
                {f:new FOSFile("IMG 374", 2803657.265067519)},
                {f:new FOSFile("IMG 375", 3166121.3876776453)},
                {f:new FOSFile("IMG 376", 3426368.624570513)},
                {f:new FOSFile("IMG 377", 622823.9420390365)},
                {f:new FOSFile("IMG 378", 3733445.5980326408)},
                {f:new FOSFile("IMG 379", 3578948.06510573)},
                {f:new FOSFile("IMG 380", 2621749.832924136)},
                {f:new FOSFile("IMG 381", 1471860.7830420295)},
                {f:new FOSFile("IMG 382", 2097373.82671701)},
                {f:new FOSFile("IMG 383", 4553396.368565865)},
                {f:new FOSFile("IMG 384", 3609520.6627592244)},
                {f:new FOSFile("IMG 385", 410571.5011252621)},
                {f:new FOSFile("IMG 386", 388087.5600643147)},
                {f:new FOSFile("IMG 387", 3675849.234780982)},
                {f:new FOSFile("IMG 388", 1557579.1374159204)},
                {f:new FOSFile("IMG 389", 3634239.815562195)},
                {f:new FOSFile("IMG 390", 4340901.231249538)},
                {f:new FOSFile("IMG 391", 3798942.990349642)},
                {f:new FOSFile("IMG 392", 3635471.6471618577)},
                {f:new FOSFile("IMG 393", 2978235.4034811505)},
                {f:new FOSFile("IMG 394", 1522388.5426518226)},
                {f:new FOSFile("IMG 395", 4703458.076902311)},
                {f:new FOSFile("IMG 396", 2258565.444508877)},
                {f:new FOSFile("IMG 397", 2241272.8622277156)},
                {f:new FOSFile("IMG 398", 5202879.477823325)},
                {f:new FOSFile("IMG 399", 5118724.2344415225)},
                {f:new FOSFile("IMG 400", 4383672.675502309)},
                {f:new FOSFile("IMG 401", 16483.380413891595)},
                {f:new FOSFile("IMG 402", 864589.1262351212)},
                {f:new FOSFile("IMG 403", 4835006.046723243)},
                {f:new FOSFile("IMG 404", 3451689.000933045)},
                {f:new FOSFile("IMG 405", 3902578.923526696)},
                {f:new FOSFile("IMG 406", 4132220.918904852)},
                {f:new FOSFile("IMG 407", 1256950.2770266712)},
                {f:new FOSFile("IMG 408", 4322815.140421902)},
                {f:new FOSFile("IMG 409", 3569734.315838338)},
                {f:new FOSFile("IMG 410", 4056864.578033718)},
                {f:new FOSFile("IMG 411", 515056.6707420283)},
                {f:new FOSFile("IMG 412", 946664.123955065)},
                {f:new FOSFile("IMG 413", 4077824.0272126915)},
                {f:new FOSFile("IMG 414", 4504977.307449087)},
                {f:new FOSFile("IMG 415", 42672.75678985891)},
                {f:new FOSFile("IMG 416", 1551102.4748690745)},
                {f:new FOSFile("IMG 417", 3040344.002926596)},
                {f:new FOSFile("IMG 418", 1955109.5339223824)},
                {f:new FOSFile("IMG 419", 1177851.649754419)},
                {f:new FOSFile("IMG 420", 5098661.063511589)},
                {f:new FOSFile("IMG 421", 2076946.4773738396)},
                {f:new FOSFile("IMG 422", 885947.5597165748)},
                {f:new FOSFile("IMG 423", 1389292.6797844758)},
                {f:new FOSFile("IMG 424", 4898766.991191984)},
                {f:new FOSFile("IMG 425", 1618314.2448301506)},
                {f:new FOSFile("IMG 426", 2737325.7743258304)},
                {f:new FOSFile("IMG 427", 2666095.839146039)},
                {f:new FOSFile("IMG 428", 2578847.8871359034)},
                {f:new FOSFile("IMG 429", 3625752.3571031475)},
                {f:new FOSFile("IMG 430", 5108291.101224922)},
                {f:new FOSFile("IMG 431", 807050.5253258697)},
                {f:new FOSFile("IMG 432", 4534167.677317471)},
                {f:new FOSFile("IMG 433", 5309033.340178818)},
                {f:new FOSFile("IMG 434", 2354627.159599051)},
                {f:new FOSFile("IMG 435", 4198873.14677139)},
                {f:new FOSFile("IMG 436", 3531639.2596532046)},
                {f:new FOSFile("IMG 437", 1167433.3942656976)},
                {f:new FOSFile("IMG 438", 1627652.935389197)},
                {f:new FOSFile("IMG 439", 2830996.2641990874)},
                {f:new FOSFile("IMG 440", 469444.6818154487)},
                {f:new FOSFile("IMG 441", 784729.5439972548)},
                {f:new FOSFile("IMG 442", 4684187.804755762)},
                {f:new FOSFile("IMG 443", 4650477.490227961)},
                {f:new FOSFile("IMG 444", 3286536.872338394)},
                {f:new FOSFile("IMG 445", 2743258.157294186)},
                {f:new FOSFile("IMG 446", 3793675.226254762)},
                {f:new FOSFile("IMG 447", 4395498.966967583)},
                {f:new FOSFile("IMG 448", 1039883.2973955206)},
                {f:new FOSFile("IMG 449", 5009118.511179817)},
                {f:new FOSFile("IMG 450", 1361825.0232539668)},
                {f:new FOSFile("IMG 451", 2367091.063624313)},
                {f:new FOSFile("IMG 452", 195197.99713564265)},
                {f:new FOSFile("IMG 453", 3374064.629003386)},
                {f:new FOSFile("IMG 454", 2942312.3718714714)},
                {f:new FOSFile("IMG 455", 2167311.311589019)},
                {f:new FOSFile("IMG 456", 1966955.782424375)},
                {f:new FOSFile("IMG 457", 3134971.217744026)},
                {f:new FOSFile("IMG 458", 925903.7309629173)},
                {f:new FOSFile("IMG 459", 2587160.4550137282)},
                {f:new FOSFile("IMG 460", 68364.49892319692)},
                {f:new FOSFile("IMG 461", 3001578.9545817883)},
                {f:new FOSFile("IMG 462", 4265710.988137406)},
                {f:new FOSFile("IMG 463", 2649466.667144981)},
                {f:new FOSFile("IMG 464", 480414.19947327673)},
                {f:new FOSFile("IMG 465", 243177.21191863497)},
                {f:new FOSFile("IMG 466", 5281742.621324752)},
                {f:new FOSFile("IMG 467", 3265317.1834216095)},
                {f:new FOSFile("IMG 468", 61739.46477360404)},
                {f:new FOSFile("IMG 469", 4770596.616948742)},
                {f:new FOSFile("IMG 470", 2667851.408498082)},
                {f:new FOSFile("IMG 471", 1517358.6384654283)},
                {f:new FOSFile("IMG 472", 87035.86935373423)},
                {f:new FOSFile("IMG 473", 1136095.3680489713)},
                {f:new FOSFile("IMG 474", 4191523.286811827)},
                {f:new FOSFile("IMG 475", 384147.7441233848)},
                {f:new FOSFile("IMG 476", 1070407.8614081286)},
                {f:new FOSFile("IMG 477", 1689250.2733708986)},
                {f:new FOSFile("IMG 478", 2621034.853973418)},
                {f:new FOSFile("IMG 479", 4342416.884605288)},
                {f:new FOSFile("IMG 480", 196809.1440951858)},
                {f:new FOSFile("IMG 481", 3119612.457947988)},
                {f:new FOSFile("IMG 482", 1151103.1452545887)},
                {f:new FOSFile("IMG 483", 698883.432633639)},
                {f:new FOSFile("IMG 484", 13840.616388034223)},
                {f:new FOSFile("IMG 485", 259804.72165534066)},
                {f:new FOSFile("IMG 486", 5104011.844265358)},
                {f:new FOSFile("IMG 487", 1134783.6004593302)},
                {f:new FOSFile("IMG 488", 441698.927136062)},
                {f:new FOSFile("IMG 489", 1970787.4326496273)},
                {f:new FOSFile("IMG 490", 2366841.4112836537)},
                {f:new FOSFile("IMG 491", 4590057.964286361)},
                {f:new FOSFile("IMG 492", 2944985.9346719114)},
                {f:new FOSFile("IMG 493", 346940.47505575564)},
                {f:new FOSFile("IMG 494", 3055613.9233512403)},
                {f:new FOSFile("IMG 495", 2982176.41039658)},
                {f:new FOSFile("IMG 496", 551999.6128464907)},
                {f:new FOSFile("IMG 497", 5253670.254743646)},
                {f:new FOSFile("IMG 498", 2253827.5914413375)},
                {f:new FOSFile("IMG 499", 3665043.7731438917)},
                {f:new FOSFile("IMG 500", 5135647.154828166)},
                {f:new FOSFile("IMG 501", 3576837.5616452126)},
                {f:new FOSFile("IMG 502", 3687145.3368623583)},
                {f:new FOSFile("IMG 503", 544923.917513496)},
                {f:new FOSFile("IMG 504", 5006700.731232432)},
                {f:new FOSFile("IMG 505", 5133517.558019915)},
                {f:new FOSFile("IMG 506", 3857243.12845984)},
                {f:new FOSFile("IMG 507", 4835663.663428053)},
                {f:new FOSFile("IMG 508", 2676149.2984819324)},
                {f:new FOSFile("IMG 509", 4538288.232717062)},
                {f:new FOSFile("IMG 510", 1062121.496580841)},
                {f:new FOSFile("IMG 511", 2127536.708295744)},
                {f:new FOSFile("IMG 512", 3798694.9976456463)},
                {f:new FOSFile("IMG 513", 2162786.5831291196)},
                {f:new FOSFile("IMG 514", 1369681.5818768733)},
                {f:new FOSFile("IMG 515", 1647530.2734689144)},
                {f:new FOSFile("IMG 516", 1870007.9024403552)},
                {f:new FOSFile("IMG 517", 3630009.960255754)},
                {f:new FOSFile("IMG 518", 5049219.406486521)},
                {f:new FOSFile("IMG 519", 962782.7157286548)},
                {f:new FOSFile("IMG 520", 3672942.987463316)},
                {f:new FOSFile("IMG 521", 2389431.9387654057)},
                {f:new FOSFile("IMG 522", 3026209.1107245455)},
                {f:new FOSFile("IMG 523", 4870266.479779074)},
                {f:new FOSFile("IMG 524", 3426988.6669478705)},
                {f:new FOSFile("IMG 525", 4139059.8196374294)},
            ]},
            {dir: new Directory('Happy times'), c:[
                {f:new FOSFile("IMG 0", 1934919.3185486475)},
                {f:new FOSFile("IMG 1", 5133247.812879165)},
                {f:new FOSFile("IMG 2", 1008101.0348055566)},
                {f:new FOSFile("IMG 3", 2072986.2484910942)},
                {f:new FOSFile("IMG 4", 2402024.968556803)},
                {f:new FOSFile("IMG 5", 2454803.1949612605)},
                {f:new FOSFile("IMG 6", 898562.4610395253)},
                {f:new FOSFile("IMG 7", 5371689.731813923)},
                {f:new FOSFile("IMG 8", 4294230.8514637835)},
                {f:new FOSFile("IMG 9", 654133.1227727582)},
                {f:new FOSFile("IMG 10", 1361417.3207920354)},
                {f:new FOSFile("IMG 11", 2629999.2695168583)},
                {f:new FOSFile("IMG 12", 1077088.5753495048)},
                {f:new FOSFile("IMG 13", 1659672.6488589246)},
                {f:new FOSFile("IMG 14", 1522475.336208689)},
                {f:new FOSFile("IMG 15", 4534093.631476997)},
                {f:new FOSFile("IMG 16", 5238003.332486153)},
                {f:new FOSFile("IMG 17", 1063165.231682828)},
                {f:new FOSFile("IMG 18", 1837560.9343681869)},
                {f:new FOSFile("IMG 19", 2710800.940491628)},
                {f:new FOSFile("IMG 20", 253869.23730138692)},
                {f:new FOSFile("IMG 21", 4147392.2875616)},
                {f:new FOSFile("IMG 22", 665962.0846395647)},
                {f:new FOSFile("IMG 23", 3610179.903317979)},
                {f:new FOSFile("IMG 24", 4764335.252428652)},
                {f:new FOSFile("IMG 25", 404009.8490326369)},
                {f:new FOSFile("IMG 26", 2908494.8510690858)},
                {f:new FOSFile("IMG 27", 532017.1194687218)},
                {f:new FOSFile("IMG 28", 3777642.651668614)},
                {f:new FOSFile("IMG 29", 1648237.8211357023)},
                {f:new FOSFile("IMG 30", 1459075.1039975923)},
                {f:new FOSFile("IMG 31", 4746418.200283798)},
                {f:new FOSFile("IMG 32", 1181318.4621195295)},
                {f:new FOSFile("IMG 33", 2265966.7191410395)},
                {f:new FOSFile("IMG 34", 5242177.712035598)},
                {f:new FOSFile("IMG 35", 1274433.7437142706)},
                {f:new FOSFile("IMG 36", 3060858.081460574)},
                {f:new FOSFile("IMG 37", 4751235.829884282)},
                {f:new FOSFile("IMG 38", 4639713.16357621)},
                {f:new FOSFile("IMG 39", 1716275.1497725218)},
                {f:new FOSFile("IMG 40", 3182255.462732055)},
                {f:new FOSFile("IMG 41", 4688028.038348409)},
                {f:new FOSFile("IMG 42", 2009636.9549468218)},
                {f:new FOSFile("IMG 43", 286043.0576994463)},
                {f:new FOSFile("IMG 44", 897.4055703632711)},
                {f:new FOSFile("IMG 45", 4697489.920198743)},
                {f:new FOSFile("IMG 46", 1943970.2893063598)},
                {f:new FOSFile("IMG 47", 879379.9791770682)},
                {f:new FOSFile("IMG 48", 3006720.4976409064)},
                {f:new FOSFile("IMG 49", 1829768.6649586346)},
                {f:new FOSFile("IMG 50", 3594117.1097387904)},
                {f:new FOSFile("IMG 51", 5121578.307297225)},
                {f:new FOSFile("IMG 52", 471997.2530050001)},
                {f:new FOSFile("IMG 53", 1143233.50420812)},
                {f:new FOSFile("IMG 54", 955420.7447746024)},
                {f:new FOSFile("IMG 55", 3549388.1211149315)},
                {f:new FOSFile("IMG 56", 1396118.0126096462)},
                {f:new FOSFile("IMG 57", 4183809.7054117504)},
                {f:new FOSFile("IMG 58", 2591172.946009412)},
                {f:new FOSFile("IMG 59", 379394.1233610112)},
                {f:new FOSFile("IMG 60", 4240243.312319416)},
                {f:new FOSFile("IMG 61", 4408218.272279176)},
                {f:new FOSFile("IMG 62", 899550.5227044325)},
                {f:new FOSFile("IMG 63", 2234626.990422657)},
                {f:new FOSFile("IMG 64", 511273.87477069616)},
                {f:new FOSFile("IMG 65", 4500530.775944228)},
                {f:new FOSFile("IMG 66", 5259142.744577927)},
                {f:new FOSFile("IMG 67", 4635228.019615685)},
                {f:new FOSFile("IMG 68", 505216.61085417884)},
                {f:new FOSFile("IMG 69", 3586008.491836827)},
                {f:new FOSFile("IMG 70", 2837203.147158156)},
                {f:new FOSFile("IMG 71", 3530468.993870205)},
                {f:new FOSFile("IMG 72", 2307651.9243988125)},
                {f:new FOSFile("IMG 73", 4699049.672451862)},
                {f:new FOSFile("IMG 74", 3975972.156606208)},
                {f:new FOSFile("IMG 75", 4276199.042703852)},
                {f:new FOSFile("IMG 76", 288624.3905550249)},
                {f:new FOSFile("IMG 77", 612547.1642024929)},
                {f:new FOSFile("IMG 78", 3665092.351154025)},
                {f:new FOSFile("IMG 79", 3087305.665705419)},
                {f:new FOSFile("IMG 80", 357576.6133348513)},
                {f:new FOSFile("IMG 81", 5219535.364552941)},
                {f:new FOSFile("IMG 82", 3288105.18948318)},
                {f:new FOSFile("IMG 83", 169208.29888639256)},
                {f:new FOSFile("IMG 84", 2418247.65398273)},
                {f:new FOSFile("IMG 85", 2703411.8814681866)},
                {f:new FOSFile("IMG 86", 5006039.632226185)},
                {f:new FOSFile("IMG 87", 4952407.956687038)},
                {f:new FOSFile("IMG 88", 3265748.2506593578)},
                {f:new FOSFile("IMG 89", 34591.79472938763)},
                {f:new FOSFile("IMG 90", 2494458.2530254857)},
                {f:new FOSFile("IMG 91", 1941900.2574907537)},
                {f:new FOSFile("IMG 92", 4357375.697969758)},
                {f:new FOSFile("IMG 93", 2434687.3631252656)},
                {f:new FOSFile("IMG 94", 939970.7754562793)},
                {f:new FOSFile("IMG 95", 4574222.85992188)},
                {f:new FOSFile("IMG 96", 2226496.391395752)},
                {f:new FOSFile("IMG 97", 1881663.1419013694)},
                {f:new FOSFile("IMG 98", 2424215.619399455)},
                {f:new FOSFile("IMG 99", 5334960.193946578)},
                {f:new FOSFile("IMG 100", 2361159.556342948)},
                {f:new FOSFile("IMG 101", 779174.1076243647)},
                {f:new FOSFile("IMG 102", 2578060.3179703425)},
                {f:new FOSFile("IMG 103", 4517080.886202562)},
                {f:new FOSFile("IMG 104", 4908332.66421883)},
                {f:new FOSFile("IMG 105", 2560423.316305693)},
                {f:new FOSFile("IMG 106", 67200.63325026212)},
                {f:new FOSFile("IMG 107", 2339160.9022653876)},
                {f:new FOSFile("IMG 108", 4516366.819674516)},
                {f:new FOSFile("IMG 109", 3451472.5755797117)},
                {f:new FOSFile("IMG 110", 4667765.592890652)},
                {f:new FOSFile("IMG 111", 2373377.1962752007)},
                {f:new FOSFile("IMG 112", 1838446.663891531)},
                {f:new FOSFile("IMG 113", 239049.87127649155)},
                {f:new FOSFile("IMG 114", 1043834.5517091961)},
                {f:new FOSFile("IMG 115", 1442201.1424371924)},
                {f:new FOSFile("IMG 116", 3055412.3290465223)},
                {f:new FOSFile("IMG 117", 567870.1223546645)},
                {f:new FOSFile("IMG 118", 1121368.9005260246)},
                {f:new FOSFile("IMG 119", 298039.9357625607)},
                {f:new FOSFile("IMG 120", 5134771.0563981915)},
                {f:new FOSFile("IMG 121", 49854.477389178326)},
                {f:new FOSFile("IMG 122", 362829.10695561016)},
                {f:new FOSFile("IMG 123", 758435.7770325354)},
                {f:new FOSFile("IMG 124", 2720020.6260516266)},
                {f:new FOSFile("IMG 125", 990342.1709389389)},
                {f:new FOSFile("IMG 126", 1219273.9075049246)},
                {f:new FOSFile("IMG 127", 1632491.313176052)},
                {f:new FOSFile("IMG 128", 474816.9593151865)},
                {f:new FOSFile("IMG 129", 2181518.613674591)},
                {f:new FOSFile("IMG 130", 2746727.4681797917)},
                {f:new FOSFile("IMG 131", 5330330.519871516)},
                {f:new FOSFile("IMG 132", 4888391.153408917)},
                {f:new FOSFile("IMG 133", 2694105.9720597416)},
                {f:new FOSFile("IMG 134", 5149567.748600311)},
                {f:new FOSFile("IMG 135", 2455963.8556068433)},
                {f:new FOSFile("IMG 136", 4796791.638360293)},
                {f:new FOSFile("IMG 137", 1501181.547619512)},
                {f:new FOSFile("IMG 138", 714690.483340779)},
                {f:new FOSFile("IMG 139", 4965502.242515027)},
                {f:new FOSFile("IMG 140", 4288481.032700917)},
                {f:new FOSFile("IMG 141", 5288749.717863177)},
                {f:new FOSFile("IMG 142", 4603858.426599511)},
                {f:new FOSFile("IMG 143", 3851792.3296487913)},
                {f:new FOSFile("IMG 144", 2281966.0203046505)},
                {f:new FOSFile("IMG 145", 3355475.6454589837)},
                {f:new FOSFile("IMG 146", 784605.0089771732)},
                {f:new FOSFile("IMG 147", 2739234.809680267)},
                {f:new FOSFile("IMG 148", 4342490.781365547)},
                {f:new FOSFile("IMG 149", 4965907.546086896)},
                {f:new FOSFile("IMG 150", 2954359.9797466076)},
                {f:new FOSFile("IMG 151", 3699749.067296748)},
                {f:new FOSFile("IMG 152", 1615572.333075509)},
                {f:new FOSFile("IMG 153", 807061.7789841506)},
                {f:new FOSFile("IMG 154", 518872.64930398064)},
                {f:new FOSFile("IMG 155", 1883763.311078725)},
                {f:new FOSFile("IMG 156", 481096.9128828525)},
                {f:new FOSFile("IMG 157", 3835873.551588227)},
                {f:new FOSFile("IMG 158", 756232.0483236323)},
                {f:new FOSFile("IMG 159", 255175.0296191889)},
                {f:new FOSFile("IMG 160", 187219.9216318475)},
                {f:new FOSFile("IMG 161", 4890274.135610149)},
                {f:new FOSFile("IMG 162", 1798838.096877109)},
                {f:new FOSFile("IMG 163", 1055683.711507954)},
                {f:new FOSFile("IMG 164", 1457225.4416372604)},
                {f:new FOSFile("IMG 165", 1716851.166815503)},
                {f:new FOSFile("IMG 166", 3189307.8731652605)},
                {f:new FOSFile("IMG 167", 4894811.176478292)},
                {f:new FOSFile("IMG 168", 3402608.401250744)},
                {f:new FOSFile("IMG 169", 2135362.473660635)},
                {f:new FOSFile("IMG 170", 272079.0782204053)},
                {f:new FOSFile("IMG 171", 939272.5061078134)},
                {f:new FOSFile("IMG 172", 4102522.0774454526)},
                {f:new FOSFile("IMG 173", 3224624.878779698)},
                {f:new FOSFile("IMG 174", 3754318.6658833455)},
                {f:new FOSFile("IMG 175", 4103071.1404134226)},
                {f:new FOSFile("IMG 176", 4290673.080648842)},
                {f:new FOSFile("IMG 177", 4057.1940869372256)},
                {f:new FOSFile("IMG 178", 5293728.106801933)},
                {f:new FOSFile("IMG 179", 5035882.531263538)},
                {f:new FOSFile("IMG 180", 3917169.208174236)},
                {f:new FOSFile("IMG 181", 4295425.631992236)},
                {f:new FOSFile("IMG 182", 3995864.8019931833)},
                {f:new FOSFile("IMG 183", 5265998.446541825)},
                {f:new FOSFile("IMG 184", 758258.712467804)},
                {f:new FOSFile("IMG 185", 1540026.8114017858)},
                {f:new FOSFile("IMG 186", 3885698.312085433)},
                {f:new FOSFile("IMG 187", 3097557.4722343427)},
                {f:new FOSFile("IMG 188", 2257157.4747311226)},
                {f:new FOSFile("IMG 189", 2061824.1709833439)},
                {f:new FOSFile("IMG 190", 3567124.970065588)},
                {f:new FOSFile("IMG 191", 1302702.4412102734)},
                {f:new FOSFile("IMG 192", 3211891.4072392667)},
                {f:new FOSFile("IMG 193", 2899673.072421647)},
                {f:new FOSFile("IMG 194", 578236.1192906979)},
                {f:new FOSFile("IMG 195", 2273181.634141097)},
                {f:new FOSFile("IMG 196", 2642407.4616994653)},
                {f:new FOSFile("IMG 197", 4139620.0299502024)},
                {f:new FOSFile("IMG 198", 3768311.8923414215)},
                {f:new FOSFile("IMG 199", 1270924.5214234209)},
                {f:new FOSFile("IMG 200", 4534157.118611752)},
                {f:new FOSFile("IMG 201", 2634492.6276640627)},
                {f:new FOSFile("IMG 202", 1507141.65386502)},
                {f:new FOSFile("IMG 203", 4922821.383554436)},
                {f:new FOSFile("IMG 204", 155525.72425945193)},
                {f:new FOSFile("IMG 205", 4630566.560456128)},
                {f:new FOSFile("IMG 206", 4696453.493523627)},
                {f:new FOSFile("IMG 207", 1840502.7587508543)},
                {f:new FOSFile("IMG 208", 2536796.6102995956)},
                {f:new FOSFile("IMG 209", 2647447.430692933)},
                {f:new FOSFile("IMG 210", 4874377.274202287)},
                {f:new FOSFile("IMG 211", 4841324.379367725)},
                {f:new FOSFile("IMG 212", 937350.0144322028)},
            ]},
            {dir: new Directory('Wedding photos'), c:[
                {f:new FOSFile("IMG 0", 1934919.3185486475)},
                {f:new FOSFile("IMG 1", 5133247.812879165)},
                {f:new FOSFile("IMG 2", 1008101.0348055566)},
                {f:new FOSFile("IMG 3", 2072986.2484910942)},
                {f:new FOSFile("IMG 4", 2402024.968556803)},
                {f:new FOSFile("IMG 5", 2454803.1949612605)},
                {f:new FOSFile("IMG 6", 898562.4610395253)},
                {f:new FOSFile("IMG 7", 5371689.731813923)},
                {f:new FOSFile("IMG 8", 4294230.8514637835)},
                {f:new FOSFile("IMG 9", 654133.1227727582)},
                {f:new FOSFile("IMG 10", 1361417.3207920354)},
                {f:new FOSFile("IMG 11", 2629999.2695168583)},
                {f:new FOSFile("IMG 12", 1077088.5753495048)},
                {f:new FOSFile("IMG 13", 1659672.6488589246)},
                {f:new FOSFile("IMG 14", 1522475.336208689)},
                {f:new FOSFile("IMG 15", 4534093.631476997)},
                {f:new FOSFile("IMG 16", 5238003.332486153)},
                {f:new FOSFile("IMG 17", 1063165.231682828)},
                {f:new FOSFile("IMG 18", 1837560.9343681869)},
                {f:new FOSFile("IMG 19", 2710800.940491628)},
                {f:new FOSFile("IMG 20", 253869.23730138692)},
                {f:new FOSFile("IMG 21", 4147392.2875616)},
                {f:new FOSFile("IMG 22", 665962.0846395647)},
                {f:new FOSFile("IMG 23", 3610179.903317979)},
                {f:new FOSFile("IMG 24", 4764335.252428652)},
                {f:new FOSFile("IMG 25", 404009.8490326369)},
                {f:new FOSFile("IMG 26", 2908494.8510690858)},
                {f:new FOSFile("IMG 27", 532017.1194687218)},
                {f:new FOSFile("IMG 28", 3777642.651668614)},
                {f:new FOSFile("IMG 29", 1648237.8211357023)},
                {f:new FOSFile("IMG 30", 1459075.1039975923)},
                {f:new FOSFile("IMG 31", 4746418.200283798)},
                {f:new FOSFile("IMG 32", 1181318.4621195295)},
                {f:new FOSFile("IMG 33", 2265966.7191410395)},
                {f:new FOSFile("IMG 34", 5242177.712035598)},
                {f:new FOSFile("IMG 35", 1274433.7437142706)},
                {f:new FOSFile("IMG 36", 3060858.081460574)},
                {f:new FOSFile("IMG 37", 4751235.829884282)},
                {f:new FOSFile("IMG 38", 4639713.16357621)},
                {f:new FOSFile("IMG 39", 1716275.1497725218)},
                {f:new FOSFile("IMG 40", 3182255.462732055)},
                {f:new FOSFile("IMG 41", 4688028.038348409)},
                {f:new FOSFile("IMG 42", 2009636.9549468218)},
                {f:new FOSFile("IMG 43", 286043.0576994463)},
                {f:new FOSFile("IMG 44", 897.4055703632711)},
                {f:new FOSFile("IMG 45", 4697489.920198743)},
                {f:new FOSFile("IMG 46", 1943970.2893063598)},
                {f:new FOSFile("IMG 47", 879379.9791770682)},
                {f:new FOSFile("IMG 48", 3006720.4976409064)},
                {f:new FOSFile("IMG 49", 1829768.6649586346)},
                {f:new FOSFile("IMG 50", 3594117.1097387904)},
                {f:new FOSFile("IMG 51", 5121578.307297225)},
                {f:new FOSFile("IMG 52", 471997.2530050001)},
                {f:new FOSFile("IMG 53", 1143233.50420812)},
                {f:new FOSFile("IMG 54", 955420.7447746024)},
                {f:new FOSFile("IMG 55", 3549388.1211149315)},
                {f:new FOSFile("IMG 56", 1396118.0126096462)},
                {f:new FOSFile("IMG 57", 4183809.7054117504)},
                {f:new FOSFile("IMG 58", 2591172.946009412)},
                {f:new FOSFile("IMG 59", 379394.1233610112)},
                {f:new FOSFile("IMG 60", 4240243.312319416)},
                {f:new FOSFile("IMG 61", 4408218.272279176)},
                {f:new FOSFile("IMG 62", 899550.5227044325)},
                {f:new FOSFile("IMG 63", 2234626.990422657)},
                {f:new FOSFile("IMG 64", 511273.87477069616)},
                {f:new FOSFile("IMG 65", 4500530.775944228)},
                {f:new FOSFile("IMG 66", 5259142.744577927)},
                {f:new FOSFile("IMG 67", 4635228.019615685)},
                {f:new FOSFile("IMG 68", 505216.61085417884)},
                {f:new FOSFile("IMG 69", 3586008.491836827)},
                {f:new FOSFile("IMG 70", 2837203.147158156)},
                {f:new FOSFile("IMG 71", 3530468.993870205)},
                {f:new FOSFile("IMG 72", 2307651.9243988125)},
                {f:new FOSFile("IMG 73", 4699049.672451862)},
                {f:new FOSFile("IMG 74", 3975972.156606208)},
                {f:new FOSFile("IMG 75", 4276199.042703852)},
                {f:new FOSFile("IMG 76", 288624.3905550249)},
                {f:new FOSFile("IMG 77", 612547.1642024929)},
                {f:new FOSFile("IMG 78", 3665092.351154025)},
                {f:new FOSFile("IMG 79", 3087305.665705419)},
                {f:new FOSFile("IMG 80", 357576.6133348513)},
                {f:new FOSFile("IMG 81", 5219535.364552941)},
                {f:new FOSFile("IMG 82", 3288105.18948318)},
                {f:new FOSFile("IMG 83", 169208.29888639256)},
                {f:new FOSFile("IMG 84", 2418247.65398273)},
                {f:new FOSFile("IMG 85", 2703411.8814681866)},
                {f:new FOSFile("IMG 86", 5006039.632226185)},
                {f:new FOSFile("IMG 87", 4952407.956687038)},
                {f:new FOSFile("IMG 88", 3265748.2506593578)},
                {f:new FOSFile("IMG 89", 34591.79472938763)},
                {f:new FOSFile("IMG 90", 2494458.2530254857)},
                {f:new FOSFile("IMG 91", 1941900.2574907537)},
                {f:new FOSFile("IMG 92", 4357375.697969758)},
                {f:new FOSFile("IMG 93", 2434687.3631252656)},
                {f:new FOSFile("IMG 94", 939970.7754562793)},
                {f:new FOSFile("IMG 95", 4574222.85992188)},
                {f:new FOSFile("IMG 96", 2226496.391395752)},
                {f:new FOSFile("IMG 97", 1881663.1419013694)},
                {f:new FOSFile("IMG 98", 2424215.619399455)},
                {f:new FOSFile("IMG 99", 5334960.193946578)},
                {f:new FOSFile("IMG 100", 2361159.556342948)},
                {f:new FOSFile("IMG 101", 779174.1076243647)},
                {f:new FOSFile("IMG 102", 2578060.3179703425)},
                {f:new FOSFile("IMG 103", 4517080.886202562)},
                {f:new FOSFile("IMG 104", 4908332.66421883)},
                {f:new FOSFile("IMG 105", 2560423.316305693)},
                {f:new FOSFile("IMG 106", 67200.63325026212)},
                {f:new FOSFile("IMG 107", 2339160.9022653876)},
                {f:new FOSFile("IMG 108", 4516366.819674516)},
                {f:new FOSFile("IMG 109", 3451472.5755797117)},
                {f:new FOSFile("IMG 110", 4667765.592890652)},
                {f:new FOSFile("IMG 111", 2373377.1962752007)},
                {f:new FOSFile("IMG 112", 1838446.663891531)},
                {f:new FOSFile("IMG 113", 239049.87127649155)},
                {f:new FOSFile("IMG 114", 1043834.5517091961)},
                {f:new FOSFile("IMG 115", 1442201.1424371924)},
                {f:new FOSFile("IMG 116", 3055412.3290465223)},
                {f:new FOSFile("IMG 117", 567870.1223546645)},
                {f:new FOSFile("IMG 118", 1121368.9005260246)},
                {f:new FOSFile("IMG 119", 298039.9357625607)},
                {f:new FOSFile("IMG 120", 5134771.0563981915)},
                {f:new FOSFile("IMG 121", 49854.477389178326)},
                {f:new FOSFile("IMG 122", 362829.10695561016)},
                {f:new FOSFile("IMG 123", 758435.7770325354)},
                {f:new FOSFile("IMG 124", 2720020.6260516266)},
                {f:new FOSFile("IMG 125", 990342.1709389389)},
                {f:new FOSFile("IMG 126", 1219273.9075049246)},
                {f:new FOSFile("IMG 127", 1632491.313176052)},
                {f:new FOSFile("IMG 128", 474816.9593151865)},
                {f:new FOSFile("IMG 129", 2181518.613674591)},
                {f:new FOSFile("IMG 130", 2746727.4681797917)},
                {f:new FOSFile("IMG 131", 5330330.519871516)},
                {f:new FOSFile("IMG 132", 4888391.153408917)},
                {f:new FOSFile("IMG 133", 2694105.9720597416)},
                {f:new FOSFile("IMG 134", 5149567.748600311)},
                {f:new FOSFile("IMG 135", 2455963.8556068433)},
                {f:new FOSFile("IMG 136", 4796791.638360293)},
                {f:new FOSFile("IMG 137", 1501181.547619512)},
                {f:new FOSFile("IMG 138", 714690.483340779)},
                {f:new FOSFile("IMG 139", 4965502.242515027)},
                {f:new FOSFile("IMG 140", 4288481.032700917)},
                {f:new FOSFile("IMG 141", 5288749.717863177)},
                {f:new FOSFile("IMG 142", 4603858.426599511)},
                {f:new FOSFile("IMG 143", 3851792.3296487913)},
                {f:new FOSFile("IMG 144", 2281966.0203046505)},
                {f:new FOSFile("IMG 145", 3355475.6454589837)},
                {f:new FOSFile("IMG 146", 784605.0089771732)},
                {f:new FOSFile("IMG 147", 2739234.809680267)},
                {f:new FOSFile("IMG 148", 4342490.781365547)},
                {f:new FOSFile("IMG 149", 4965907.546086896)},
                {f:new FOSFile("IMG 150", 2954359.9797466076)},
                {f:new FOSFile("IMG 151", 3699749.067296748)},
                {f:new FOSFile("IMG 152", 1615572.333075509)},
                {f:new FOSFile("IMG 153", 807061.7789841506)},
                {f:new FOSFile("IMG 154", 518872.64930398064)},
                {f:new FOSFile("IMG 155", 1883763.311078725)},
                {f:new FOSFile("IMG 156", 481096.9128828525)},
                {f:new FOSFile("IMG 157", 3835873.551588227)},
                {f:new FOSFile("IMG 158", 756232.0483236323)},
                {f:new FOSFile("IMG 159", 255175.0296191889)},
                {f:new FOSFile("IMG 160", 187219.9216318475)},
                {f:new FOSFile("IMG 161", 4890274.135610149)},
                {f:new FOSFile("IMG 162", 1798838.096877109)},
                {f:new FOSFile("IMG 163", 1055683.711507954)},
                {f:new FOSFile("IMG 164", 1457225.4416372604)},
                {f:new FOSFile("IMG 165", 1716851.166815503)},
                {f:new FOSFile("IMG 166", 3189307.8731652605)},
                {f:new FOSFile("IMG 167", 4894811.176478292)},
                {f:new FOSFile("IMG 168", 3402608.401250744)},
                {f:new FOSFile("IMG 169", 2135362.473660635)},
                {f:new FOSFile("IMG 170", 272079.0782204053)},
                {f:new FOSFile("IMG 171", 939272.5061078134)},
                {f:new FOSFile("IMG 172", 4102522.0774454526)},
            ]},
        ]},
        {dir: new Directory('MyReceivedFiles'), c:[

        ]},
        {dir: new Directory('Desktop'), c:[

        ]},
        {dir: new Directory('Music'), c:[
            {dir: new Directory('My Favourite Feel Good Playlist'), c:[
                {f:new FOSFile("Track 0", 20331418.04049702)},
                {f:new FOSFile("Track 1", 28200867.266993944)},
                {f:new FOSFile("Track 2", 24555781.905544307)},
                {f:new FOSFile("Track 3", 23896741.319323268)},
                {f:new FOSFile("Track 4", 8009124.659967275)},
                {f:new FOSFile("Track 5", 14969129.994878596)},
                {f:new FOSFile("Track 6", 20845799.327365544)},
                {f:new FOSFile("Track 7", 28830264.724562544)},
                {f:new FOSFile("Track 8", 21984747.4778278)},
                {f:new FOSFile("Track 9", 26897211.51108133)},
                {f:new FOSFile("Track 10", 6022406.303764421)},
                {f:new FOSFile("Track 11", 20212504.20558309)},
                {f:new FOSFile("Track 12", 23345205.380601384)},
                {f:new FOSFile("Track 13", 5275974.621814956)},
                {f:new FOSFile("Track 14", 1715845.8038755858)},
                {f:new FOSFile("Track 15", 8468209.478878867)},
                {f:new FOSFile("Track 16", 18687260.871082906)},
                {f:new FOSFile("Track 17", 28714695.512851235)},
                {f:new FOSFile("Track 18", 30609564.139245912)},
                {f:new FOSFile("Track 19", 618578.9859298494)},
                {f:new FOSFile("Track 20", 29615025.909833923)},
                {f:new FOSFile("Track 21", 2994370.165952281)},
                {f:new FOSFile("Track 22", 30753362.29394073)},
                {f:new FOSFile("Track 23", 8724954.030306997)},
                {f:new FOSFile("Track 24", 6546540.349772754)},
                {f:new FOSFile("Track 25", 24938513.916862182)},
                {f:new FOSFile("Track 26", 14360502.502780119)},
                {f:new FOSFile("Track 27", 28125009.624542538)},
                {f:new FOSFile("Track 28", 18952653.120718155)},
                {f:new FOSFile("Track 29", 21425875.961410023)},
                {f:new FOSFile("Track 30", 8759694.240480559)},
                {f:new FOSFile("Track 31", 25613481.403725885)},
                {f:new FOSFile("Track 32", 13579348.624256749)},
                {f:new FOSFile("Track 33", 19940687.09403513)},
                {f:new FOSFile("Track 34", 1560190.8457705895)},
                {f:new FOSFile("Track 35", 21544617.818707775)},
                {f:new FOSFile("Track 36", 867441.3798467763)},
                {f:new FOSFile("Track 37", 28431002.155537453)},
                {f:new FOSFile("Track 38", 21267477.48765806)},
                {f:new FOSFile("Track 39", 18998755.65669522)},
                {f:new FOSFile("Track 40", 999830.529670154)},
                {f:new FOSFile("Track 41", 12707566.40568143)},
                {f:new FOSFile("Track 42", 6106035.996764765)},
                {f:new FOSFile("Track 43", 29306169.53825343)},
                {f:new FOSFile("Track 44", 89241.41982485949)},
                {f:new FOSFile("Track 45", 64239.23233687739)},
                {f:new FOSFile("Track 46", 14606182.585785164)},
                {f:new FOSFile("Track 47", 6863967.559291899)},
                {f:new FOSFile("Track 48", 22659371.05618365)},
                {f:new FOSFile("Track 49", 29414868.974377543)},
                {f:new FOSFile("Track 50", 28316385.511537686)},
                {f:new FOSFile("Track 51", 6299253.862032349)},
                {f:new FOSFile("Track 52", 4962283.508610057)},
                {f:new FOSFile("Track 53", 16243278.931109348)},
                {f:new FOSFile("Track 54", 26130622.507869836)},
                {f:new FOSFile("Track 55", 13109540.615850236)},
                {f:new FOSFile("Track 56", 27181698.373328168)},
                {f:new FOSFile("Track 57", 924449.148564597)},
                {f:new FOSFile("Track 58", 17890020.39373208)},
                {f:new FOSFile("Track 59", 25751817.465881832)},
                {f:new FOSFile("Track 60", 2429991.620777116)},
                {f:new FOSFile("Track 61", 12391498.996601263)},
                {f:new FOSFile("Track 62", 7682777.001641329)},
                {f:new FOSFile("Track 63", 3348454.616698463)},
                {f:new FOSFile("Track 64", 6818099.9681683285)},
                {f:new FOSFile("Track 65", 11717085.027392162)},
                {f:new FOSFile("Track 66", 27322780.04201386)},
                {f:new FOSFile("Track 67", 23495625.766543165)},
                {f:new FOSFile("Track 68", 25609672.12602737)},
                {f:new FOSFile("Track 69", 29591346.489262283)},
                {f:new FOSFile("Track 70", 10576864.079935508)},
                {f:new FOSFile("Track 71", 5453569.057677993)},
                {f:new FOSFile("Track 72", 15551795.79547937)},
                {f:new FOSFile("Track 73", 14635890.182161517)},
                {f:new FOSFile("Track 74", 1051537.6848462734)},
                {f:new FOSFile("Track 75", 24808055.161079943)},
                {f:new FOSFile("Track 76", 3992577.026560101)},
                {f:new FOSFile("Track 77", 23298912.042966403)},
                {f:new FOSFile("Track 78", 25503996.781746116)},
                {f:new FOSFile("Track 79", 20937380.254461013)},
                {f:new FOSFile("Track 80", 15461576.771805223)},
                {f:new FOSFile("Track 81", 4014843.7231089864)},
                {f:new FOSFile("Track 82", 6113307.518865604)},
                {f:new FOSFile("Track 83", 20078627.75625204)},
                {f:new FOSFile("Track 84", 23108930.64192819)},
                {f:new FOSFile("Track 85", 8661943.466358915)},
                {f:new FOSFile("Track 86", 30611092.449952938)},
                {f:new FOSFile("Track 87", 6256384.813191301)},
                {f:new FOSFile("Track 88", 16717410.548356598)},
                {f:new FOSFile("Track 89", 24470813.935942587)},
                {f:new FOSFile("Track 90", 24398347.414593372)},
                {f:new FOSFile("Track 91", 208900.0198588451)},
                {f:new FOSFile("Track 92", 22813650.376548283)},
                {f:new FOSFile("Track 93", 8026327.684677133)},
                {f:new FOSFile("Track 94", 384295.7863615693)},
                {f:new FOSFile("Track 95", 29178443.84312064)},
                {f:new FOSFile("Track 96", 2071020.2452169429)},
                {f:new FOSFile("Track 97", 16638564.68648093)},
                {f:new FOSFile("Track 98", 2704276.154331586)},
                {f:new FOSFile("Track 99", 11578153.474960953)},
                {f:new FOSFile("Track 100", 30750322.926773664)},
                {f:new FOSFile("Track 101", 16816803.541229226)},
                {f:new FOSFile("Track 102", 27383855.192585357)},
                {f:new FOSFile("Track 103", 27272225.003873102)},
                {f:new FOSFile("Track 104", 22134830.363830414)},
                {f:new FOSFile("Track 105", 12134092.966084754)},
                {f:new FOSFile("Track 106", 1347251.9083184365)},
                {f:new FOSFile("Track 107", 28160094.108400326)},
                {f:new FOSFile("Track 108", 28208983.677629784)},
                {f:new FOSFile("Track 109", 28629810.016818862)},
                {f:new FOSFile("Track 110", 7873485.729734049)},
                {f:new FOSFile("Track 111", 9670690.203270182)},
                {f:new FOSFile("Track 112", 13909233.46091091)},
                {f:new FOSFile("Track 113", 18056876.321463417)},
                {f:new FOSFile("Track 114", 10946153.534101287)},
                {f:new FOSFile("Track 115", 14383004.054378318)},
                {f:new FOSFile("Track 116", 20276187.73407958)},
                {f:new FOSFile("Track 117", 12105084.15534132)},
                {f:new FOSFile("Track 118", 15968815.411736868)},
                {f:new FOSFile("Track 119", 21644625.23891908)},
                {f:new FOSFile("Track 120", 19754736.28518436)},
                {f:new FOSFile("Track 121", 6087871.112151482)},
                {f:new FOSFile("Track 122", 25968384.523016598)},
                {f:new FOSFile("Track 123", 10787865.512932649)},
                {f:new FOSFile("Track 124", 23326134.93025129)},
                {f:new FOSFile("Track 125", 387747.91989159805)},
                {f:new FOSFile("Track 126", 16904162.665707525)},
                {f:new FOSFile("Track 127", 17193354.0167701)},
                {f:new FOSFile("Track 128", 2226432.832317837)},
                {f:new FOSFile("Track 129", 29116051.894946385)},
                {f:new FOSFile("Track 130", 30819317.290294673)},
                {f:new FOSFile("Track 131", 24159639.403562196)},
                {f:new FOSFile("Track 132", 9830014.146991987)},
                {f:new FOSFile("Track 133", 10486078.622862075)},
                {f:new FOSFile("Track 134", 30596815.9454737)},
                {f:new FOSFile("Track 135", 28012059.755350437)},
                {f:new FOSFile("Track 136", 13371889.978209486)},
                {f:new FOSFile("Track 137", 6488216.302520097)},
                {f:new FOSFile("Track 138", 30588061.16258827)},
                {f:new FOSFile("Track 139", 10846233.931763921)},
                {f:new FOSFile("Track 140", 23412059.715497468)},
                {f:new FOSFile("Track 141", 4763484.988902087)},
                {f:new FOSFile("Track 142", 25035147.05839578)},
                {f:new FOSFile("Track 143", 29701893.268388063)},
                {f:new FOSFile("Track 144", 25018114.04257585)},
                {f:new FOSFile("Track 145", 2848900.3426307263)},
                {f:new FOSFile("Track 146", 28810981.590604577)},
                {f:new FOSFile("Track 147", 4516999.485503717)},
                {f:new FOSFile("Track 148", 12404079.259720461)},
                {f:new FOSFile("Track 149", 17418932.46403013)},
                {f:new FOSFile("Track 150", 25989525.723153718)},
                {f:new FOSFile("Track 151", 4346043.537964402)},
                {f:new FOSFile("Track 152", 28178898.519175675)},
                {f:new FOSFile("Track 153", 8007341.499062556)},
                {f:new FOSFile("Track 154", 11982203.521164231)},
                {f:new FOSFile("Track 155", 28673389.29700996)},
                {f:new FOSFile("Track 156", 10891185.410465537)},
                {f:new FOSFile("Track 157", 18670153.802411184)},
                {f:new FOSFile("Track 158", 3488679.3305320847)},
                {f:new FOSFile("Track 159", 12630457.01560863)},
                {f:new FOSFile("Track 160", 16213085.912727658)},
                {f:new FOSFile("Track 161", 14598823.851147706)},
                {f:new FOSFile("Track 162", 13432945.338139312)},
                {f:new FOSFile("Track 163", 5240864.384662299)},
                {f:new FOSFile("Track 164", 15295800.705747498)},
                {f:new FOSFile("Track 165", 24227660.553877436)},
                {f:new FOSFile("Track 166", 6377650.818620477)},
                {f:new FOSFile("Track 167", 28796329.34294068)},
                {f:new FOSFile("Track 168", 24925331.887446497)},
                {f:new FOSFile("Track 169", 26591801.251433372)},
                {f:new FOSFile("Track 170", 20177946.8680044)},
                {f:new FOSFile("Track 171", 18345153.485946137)},
                {f:new FOSFile("Track 172", 6384337.250768746)},
                {f:new FOSFile("Track 173", 9481189.707386924)},
                {f:new FOSFile("Track 174", 23245363.208501253)},
                {f:new FOSFile("Track 175", 13620075.975826047)},
                {f:new FOSFile("Track 176", 11105389.848559704)},
                {f:new FOSFile("Track 177", 9683073.718469806)},
                {f:new FOSFile("Track 178", 29894489.023859072)},
                {f:new FOSFile("Track 179", 27183338.64325062)},
                {f:new FOSFile("Track 180", 8182809.359114977)},
                {f:new FOSFile("Track 181", 3534007.352217387)},
                {f:new FOSFile("Track 182", 31128517.474476833)},
                {f:new FOSFile("Track 183", 10545457.875467245)},
                {f:new FOSFile("Track 184", 11431425.485369109)},
                {f:new FOSFile("Track 185", 6662042.589736529)},
                {f:new FOSFile("Track 186", 2065145.3944138947)},
                {f:new FOSFile("Track 187", 150471.8667686065)},
                {f:new FOSFile("Track 188", 18154652.58944635)},
                {f:new FOSFile("Track 189", 7587604.948408024)},
                {f:new FOSFile("Track 190", 24241402.520796828)},
                {f:new FOSFile("Track 191", 18438222.438789375)},
                {f:new FOSFile("Track 192", 10412051.392725755)},
                {f:new FOSFile("Track 193", 7416232.884355713)},
                {f:new FOSFile("Track 194", 10758356.597179882)},
                {f:new FOSFile("Track 195", 3550685.991417335)},
                {f:new FOSFile("Track 196", 8743379.896484077)},
                {f:new FOSFile("Track 197", 23530783.905261643)},
                {f:new FOSFile("Track 198", 27762139.48057234)},
                {f:new FOSFile("Track 199", 4047727.266931002)},
                {f:new FOSFile("Track 200", 1497081.3666104276)},
                {f:new FOSFile("Track 201", 19692677.121317428)},
                {f:new FOSFile("Track 202", 6641071.968851543)},
                {f:new FOSFile("Track 203", 22872581.166581348)},
                {f:new FOSFile("Track 204", 24888081.96720453)},
                {f:new FOSFile("Track 205", 4082658.7378640734)},
                {f:new FOSFile("Track 206", 19183056.463420957)},
                {f:new FOSFile("Track 207", 21928885.852855135)},
                {f:new FOSFile("Track 208", 8727004.586509362)},
                {f:new FOSFile("Track 209", 22829041.616984338)},
                {f:new FOSFile("Track 210", 21784343.02307817)},
                {f:new FOSFile("Track 211", 29719846.270297293)},
                {f:new FOSFile("Track 212", 26483691.780023586)},
                {f:new FOSFile("Track 213", 6953121.654730331)},
                {f:new FOSFile("Track 214", 13057299.290463008)},
                {f:new FOSFile("Track 215", 14635141.59132894)},
                {f:new FOSFile("Track 216", 11456144.476361215)},
                {f:new FOSFile("Track 217", 21695848.443264302)},
                {f:new FOSFile("Track 218", 30004688.85629118)},
                {f:new FOSFile("Track 219", 26245234.13460325)},
                {f:new FOSFile("Track 220", 4672639.03232716)},
                {f:new FOSFile("Track 221", 10861277.266248014)},
                {f:new FOSFile("Track 222", 27748350.475111216)},
                {f:new FOSFile("Track 223", 16218315.156743594)},
                {f:new FOSFile("Track 224", 11208630.290147936)},
                {f:new FOSFile("Track 225", 21155532.034147006)},
                {f:new FOSFile("Track 226", 17526322.712258797)},
                {f:new FOSFile("Track 227", 30291568.112585563)},
                {f:new FOSFile("Track 228", 8683670.117897986)},
                {f:new FOSFile("Track 229", 18150011.092854746)},
                {f:new FOSFile("Track 230", 8878029.466192592)},
                {f:new FOSFile("Track 231", 2521516.2775981985)},
                {f:new FOSFile("Track 232", 30201668.015955873)},
                {f:new FOSFile("Track 233", 12660755.384573597)},
                {f:new FOSFile("Track 234", 18076074.159055036)},
                {f:new FOSFile("Track 235", 21868666.6440033)},
                {f:new FOSFile("Track 236", 9703400.160439461)},
                {f:new FOSFile("Track 237", 2556607.9387712944)},
                {f:new FOSFile("Track 238", 6909502.75804836)},
                {f:new FOSFile("Track 239", 22977150.051111333)},
                {f:new FOSFile("Track 240", 29697224.149151336)},
                {f:new FOSFile("Track 241", 8631948.87683215)},
                {f:new FOSFile("Track 242", 18877987.133493807)},
                {f:new FOSFile("Track 243", 15361647.32584141)},
                {f:new FOSFile("Track 244", 13957244.468800668)},
                {f:new FOSFile("Track 245", 15621548.853147548)},
                {f:new FOSFile("Track 246", 23336947.199033163)},
                {f:new FOSFile("Track 247", 26860174.13812009)},
                {f:new FOSFile("Track 248", 31329192.224607717)},
                {f:new FOSFile("Track 249", 11020270.669967536)},
                {f:new FOSFile("Track 250", 819822.8891967515)},
                {f:new FOSFile("Track 251", 18091335.14382321)},
                {f:new FOSFile("Track 252", 1018540.6323022713)},
                {f:new FOSFile("Track 253", 15197939.09832369)},
                {f:new FOSFile("Track 254", 24083820.90174394)},
                {f:new FOSFile("Track 255", 16081997.665184315)},
                {f:new FOSFile("Track 256", 8327604.872774108)},
                {f:new FOSFile("Track 257", 25127093.030497264)},
                {f:new FOSFile("Track 258", 25476679.897869773)},
                {f:new FOSFile("Track 259", 2767372.4860982182)},
                {f:new FOSFile("Track 260", 29343473.71732807)},
                {f:new FOSFile("Track 261", 20727832.73620357)},
                {f:new FOSFile("Track 262", 5364245.507255239)},
                {f:new FOSFile("Track 263", 8522276.903275827)},
                {f:new FOSFile("Track 264", 28761497.515956473)},
                {f:new FOSFile("Track 265", 21427994.19215312)},
                {f:new FOSFile("Track 266", 2248219.9599551195)},
                {f:new FOSFile("Track 267", 12319807.711344749)},
                {f:new FOSFile("Track 268", 18382514.886030428)},
                {f:new FOSFile("Track 269", 28769298.27307868)},
                {f:new FOSFile("Track 270", 3688734.2192059704)},
                {f:new FOSFile("Track 271", 17654766.834423784)},
                {f:new FOSFile("Track 272", 17828157.159557275)},
                {f:new FOSFile("Track 273", 16850405.98525339)},
                {f:new FOSFile("Track 274", 13438720.927290896)},
                {f:new FOSFile("Track 275", 25454279.13036904)},
                {f:new FOSFile("Track 276", 20901180.703293156)},
                {f:new FOSFile("Track 277", 15111498.40577653)},
                {f:new FOSFile("Track 278", 19197.90599089405)},
                {f:new FOSFile("Track 279", 25930739.3270927)},
                {f:new FOSFile("Track 280", 30636414.388874322)},
                {f:new FOSFile("Track 281", 22609467.214978255)},
                {f:new FOSFile("Track 282", 21544465.75789189)},
                {f:new FOSFile("Track 283", 23409614.665259268)},
                {f:new FOSFile("Track 284", 23012585.784054566)},
                {f:new FOSFile("Track 285", 16591492.030964514)},
                {f:new FOSFile("Track 286", 30829325.869293306)},
                {f:new FOSFile("Track 287", 24560720.31483928)},
                {f:new FOSFile("Track 288", 24184124.393368613)},
                {f:new FOSFile("Track 289", 26528362.280250836)},
                {f:new FOSFile("Track 290", 6366752.9812260885)},
                {f:new FOSFile("Track 291", 25706822.39681177)},
                {f:new FOSFile("Track 292", 25857010.857482333)},
                {f:new FOSFile("Track 293", 3538928.656687805)},
                {f:new FOSFile("Track 294", 4959394.7610094)},
                {f:new FOSFile("Track 295", 26780765.60819676)},
                {f:new FOSFile("Track 296", 20700921.68196666)},
                {f:new FOSFile("Track 297", 19510698.692977615)},
                {f:new FOSFile("Track 298", 2362867.2947736513)},
                {f:new FOSFile("Track 299", 1285902.4744971704)},
                {f:new FOSFile("Track 300", 13355246.342580128)},
                {f:new FOSFile("Track 301", 28001709.598071855)},
                {f:new FOSFile("Track 302", 19721725.96430118)},
                {f:new FOSFile("Track 303", 2221788.5277560055)},
                {f:new FOSFile("Track 304", 20865400.73854339)},
                {f:new FOSFile("Track 305", 5108697.961718857)},
                {f:new FOSFile("Track 306", 23552142.975167125)},
                {f:new FOSFile("Track 307", 15373612.232824065)},
                {f:new FOSFile("Track 308", 13514022.1832666)},
                {f:new FOSFile("Track 309", 3165097.5037846356)},
                {f:new FOSFile("Track 310", 27538114.79950997)},
                {f:new FOSFile("Track 311", 9424319.243529124)},
                {f:new FOSFile("Track 312", 24805432.010167185)},
                {f:new FOSFile("Track 313", 7915808.6387890205)},
                {f:new FOSFile("Track 314", 5069715.20387352)},
                {f:new FOSFile("Track 315", 20127370.68672782)},
                {f:new FOSFile("Track 316", 14491797.482507141)},
                {f:new FOSFile("Track 317", 25666483.883870352)},
                {f:new FOSFile("Track 318", 24193047.691780295)},
                {f:new FOSFile("Track 319", 6316038.978192881)},
                {f:new FOSFile("Track 320", 29215290.39881701)},
                {f:new FOSFile("Track 321", 16347280.184683777)},
                {f:new FOSFile("Track 322", 15388910.867551524)},
                {f:new FOSFile("Track 323", 13607913.704167916)},
                {f:new FOSFile("Track 324", 11546148.624004047)},
                {f:new FOSFile("Track 325", 6843283.244140054)},
                {f:new FOSFile("Track 326", 13919904.957179794)},
                {f:new FOSFile("Track 327", 20919939.89940075)},
                {f:new FOSFile("Track 328", 10821425.942755675)},
                {f:new FOSFile("Track 329", 23743695.29906793)},
                {f:new FOSFile("Track 330", 3112628.0852655815)},
                {f:new FOSFile("Track 331", 12941173.300463906)},
                {f:new FOSFile("Track 332", 31264502.173480634)},
                {f:new FOSFile("Track 333", 14181556.460684849)},
                {f:new FOSFile("Track 334", 11656059.13917134)},
                {f:new FOSFile("Track 335", 24919303.498088617)},
                {f:new FOSFile("Track 336", 23363727.41511358)},
                {f:new FOSFile("Track 337", 13192150.835785756)},
                {f:new FOSFile("Track 338", 6548301.987626163)},
                {f:new FOSFile("Track 339", 16511673.413777336)},
                {f:new FOSFile("Track 340", 870149.7097562694)},
                {f:new FOSFile("Track 341", 26531170.857932404)},
            ]},
            {f: new FOSFile("Never being given up.mp3", 14920000)},
        ]},
        {dir: new Directory('Share'), c:[

        ]},
        {dir: new Directory('Public'), c:[

        ]},
        {dir: new Directory('Videos'), c:[
            {f: new FOSFile("Happy Cats Jumping Around.mp4", 367410000)},
            {f: new FOSFile("Funny Meme.webm", 83100000)},
            {f: new FOSFile("Son's first birthday.mp4", 8320000000)},
            {f: new FOSFile("Graduation party.mp4", 4470000000)},
            {f: new FOSFile("Grandma.mp4", 5183000000)},
            {f: new FOSFile("Puppy adoption.mp4", 926000000)},
            {f: new FOSFile("Family vacation.mp4", 12260000000)},
        ]},
        {dir: new Directory('Programs'), c:[
            {f: new FOSFile("Desktop Pet.exe", 65100000)},
            {f: new FOSFile("Cursor changer.exe", 81920000)},
            {f: new FOSFile("Work Program Suite Pro.exe", 1604920000)},
            {f: new FOSFile("Game Network.exe", 1424920000)},
        ]},
    ]});
    
    emailApp(rootdir);

    makeBrowser(rootdir);

    const explorerWin = new FOSWindow(`Files`, {x:`40em`, y:`30em`}, {x:`${Math.random()*20}em`, y:`${Math.random()*20}em`}, fileExplorer, 'img/folder.png');
    explorerWin.attach(windowGroup, taskBar, desktop, windowList);
    windowList.push(explorerWin);

    getEmail('Me', 'My tasks for today', 'Install Unbelievable Engine, so I can finall start gamedev :) <br/> Do note that this game is entirely fictional, any similarities real life companies and products are entirely coinicidental. <br/> Brought to you by trauma. <br/> <br/> Made by <a href="https://kinami.dev">Kinami Imai (今井きなみ)</a>')
}
